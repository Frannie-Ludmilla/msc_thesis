## Content ##

LaTeX sources of my MSc Thesis. Latest pushed update (PDF): 30th July 2016.


## Keywords ##
Big Data, Collaboration , Distributed systems, *Akka*, *Apache Spark*, *Apache Flink*, Ensemble Machine Learning  

## Code Repositories ##
See related source code repos:

- akka project @

- Apache Flink simple KNN @

- Apache Spark simple KNN @ 



## Abstract ##

Performing Machine Learning tasks on distributed data is a challenge because we have
to shift from a centralized paradigm to a distributed one in order to allow the learner to
train itself on the largest possible amount of data. There are two main issues that prevent
us from simply collecting in one place all the data scattered in plenty of independent
and geodistributed data nodes: first of all, sensitive information could be stored there
whose transferring is strongly discouraged or not even allowed and, lastly, communication
boundaries preclude the transmission of sizeable datasets in reasonable time.

Many distributed machine learning implementations share high-level insights gathered
from local computations on condition that all the nodes are provided with the same
software environment because usually just one monolithic program is entrusted with the
actual data processing and the subsequent information exchange.
However, since different nodes are presumably equipped with a range of disparate software
ecosystems and hardware architectures, this should encourage us to favour interoperability
by blending in these local insights independently of the individual infrastructure of each
node.

Our contribution involves the implementation of a cluster that decouples the intercon-
nection logic from the actual processing, allowing therefore data exchange even in the
case of heterogeneous technologies. The binding of independent physical nodes into one
logical cluster is achieved through a lightweight application in Akka.
By exploiting the straightforward k-nearest neighbours algorithm, we arranged the clas-
sification task such that no details of the datasets are revealed. We analyse how with
this approach we can ensure an accuracy level comparable to the one we would have had
with all the data residing in one place and we show that, in case of sizeable datasets, a
distributed execution leads also to an increase in performance compared with a centralized
computation.