\begin{savequote}[65mm]
	\textquotedblleft Wisdom is one thing.\\ It is to know the thought by which \\ all things are steered through all things.\textquotedblright
	\qauthor{Heraclitus, \emph{Fragment no 41}}
\end{savequote}
\chapter{Introduction}
\section{A data-centered world}
%\epigraph{The hidden harmony is better than the visible}%
%{Heraclitus  - \emph{Fragment no 41}}
Internet has allowed the everyday man and woman to switch his/her role from a passive consumer to an active producer: this information deluge prompted IT infrastructures to comply with the need for storing huge amounts of heterogeneous data coming fast.\\
We tend to forget that in addition to all those contents voluntarily produced, a large amount of metadata and automatically generated data is being produced as well and that it could be even more revealing than the data they are about: let us think for example at the importance of logs in Business Intelligence.\\
The widely quoted definition of Big Data by Gartner refers to it as a huge avalanche (\textit{Volume}) of data in which structured and unstructured inputs are intertwined (\textit{Variety}) and that is coming at a fast pace (\textit{Velocity}).\\
%The three ideas of \textit{Volume}, \textit{Velocity} and  were intertwined into Gartner's widely quoted definition of Big Data.\\
%We are urged to interpret this data avalanche whose meaning we desperately need to find or we will not be able to take advantage of it.\\
If we do not properly handle this avalanche, we will not be able to make the most of it: from an enterprise point of view, it is straightforward to say that lucrative chances are lost.\\
To tackle these shortcomings, new Big Data technologies were developed that take care not only of handling and storing the generated binary flood but also of harvesting it in search of recognizable patterns such that we can make predictions based on them.\\
Lots of tools have been devised that offer their own solution to Big Data problems: we cannot state that one appliance is better than the others because an appliance's approach may achieve the best possible performance in a particular context while proving completely ineffective in others.
Having said that, it is quite understandable why there is no \textit{de facto} standard in Big Data: each data-centre adopts the frameworks that are better tailored to its demands thus we end up with a widely diversified environment whose interoperability cannot be ascertained.\\
Why should we want interoperability?\\
Machine Learning techniques are useful not only for producing well thought out predictions to exploit in targeted advertisement or in whatever money-making purpose but they can also be applied for the greater good as it happens in scientific research. For example, could we find patterns to show us that an illness is more likely to strike someone given some health indicators?\\
Our thoughts go straight to the records of people treated in hospitals: we would like to ask for proofs to support our theories without harnessing the patients' privacy.\\
Therefore, the simplest and most effective approaches suggest us not to transfer data out of the hospitals' premises and to sanitize properly what will be handed over as a response.\\
This problem is called \textbf{Privacy Preserving Distributed Learning} but it is not the only reason that could force us to perform computation \textit{in loco}: we could have also massive datasets without any confidentiality requirement whose size simply makes transmission not viable.\\
This thesis aims to bind together Distributed Systems, Big Data and Machine Learning techniques to guarantee cooperation between independent data centres while surveying the issues we can incur into.

\section{Goals and motivation}
This project's purpose is to prove how it is possible to build a reliable distributed system that allows cooperation in Machine Learning tasks between different data centers independently of their installed frameworks and architectures.\\
The cluster architecture that we propose targets those \textbf{ensemble methods} (see \S \ref{DistribLearning}) that put together predictions coming from different learners: since we do not want to endanger the sensitive information stored in the datasets, we opt for local processing and we combine the obtained intermediate outcomes in order to carry out our classification task.\\
%that can run their own independent computations via their installed frameworks without giving away too much details of their private data.\\
A (must-read) insightful starting point was the paper \textit{Distributed Learning, Communication Complexity and Privacy} by Balcan \cite{Balcan_theory}: as they point out, even for the case of just two entities $A$ and $B$, an information exchange during the learning process is strongly advised because we may have biased datasets (e.g. all the positive instances in $A$ and all the negative in $B$ ) that could lead us to very inaccurate learners.\\
We can summarize the main issues inside the \textbf{distributed learning} field with these three problems:
\begin{enumerate}
	\itemsep0em
	\item Creating algorithms that can perform well in a distributed fashion and with substantial data bulks;
	\item Implementing a protocol or a distributed system appliance to link clusters that can be far away both logically and physically (geographically apart) in a resilient way;
	\item Establishing how data should be formatted such that the exchange works independently of the adopted framework and that learning is possible without endangering privacy.
\end{enumerate}
Given the diversified reality in terms of installed frameworks and cluster architectures, each one with its own security and remote configurations, my proposal is simple: \textbf{do not reinvent the square wheel}.\\
As a matter of fact, there are two open source frameworks, \emph{Apache Spark} and \emph{Apache Flink}, that have amazing performances (and libraries) when it comes down to carrying out tasks in gigantic datasets (see Chapter \ref{CH_BD}) and there is a toolkit (\emph{Akka}, see Chapter \ref{CH_AKKA}) that is rapidly becoming the new golden standard for reactive programming and for building elastic and resilient distributed systems.\\
Unfortunately, the most substantial drawback is the steep learning curve for Scala, the preferred language for the frameworks\footnote{Although \emph{Apache Spark} and \emph{Apache Flink} allow programmers to use also Java and Python, the former is not advised because of its verbosity while the latter is not fully compatible with all the libraries and its syntax for lambda functions is way less straightforward than Scala's. Moreover \emph{Apache Spark} and \emph{Apache Flin}k internals are actually written in Scala}, and for \emph{Akka}: there are still very few published manuals about Akka and programmers have to skim through a whopping technical documentation.\\
In any case, our efforts should be put on finding a way for these now ubiquitous tools to interoperate smoothly rather than writing a new monolithic software that will have to face the challenges that continuous software deployment brings about.

\section{Employed technologies}
The cooperative \textit{logical} cluster interconnecting the data centers will be built in \emph{Akka}, the intended classification algorithm will be run on \emph{Apache Spark} and \emph{Apache Flink} and the data processing outcome will be sanitized in order to preserve privacy during the transmission.\\
I have used two publicly available dataset from the UCI Machine Learning Repository\footnote{\url{http://archive.ics.uci.edu/ml/index.html}} fit for classification: the first one is related to the Thyroid Disease with 7'200 instances and 21 attributes\footnote{Available at the UCI Repository \url{http://archive.ics.uci.edu/ml/datasets/Thyroid+Disease}} and the second is the SUperSYmmetrical (SUSY) particles'  dataset\footnote{Available at the UCI Repository \url{https://archive.ics.uci.edu/ml/datasets/SUSY}} that contains the result of 5'000'000 collisions -produced at the Large Hadron Collider (LHC)- to test the performance with a sizeable input.\\
Most of the development was done in \emph{Scala} because all the used frameworks were based on it. As a matter of fact, the \emph{Scala} \textsc{Actor model} (more at \S \ref{akka_actors}) was the foundation for the \emph{Akka} counterpart.
Moreover \emph{Scala}'s functional paradigm was an excellent fit for Big Data applications and its JVM-based nature allowed full interoperability with the \emph{Apache Hadoop} environment and the third-party libraries usually employed with it.\\
Both the scripting\footnote{e.g. Passing the configuration during the cluster initialization, watching and storing the query results shown in Chapter \ref{conclusion_chapter}} and the plotting of most of the graphs were developed in Python: \textsf{matplotlib} used in combination with \textsf{pandas} was very straightforward even for people like me who have just a beginner level in Python.

\section{Thesis' structure}
Since this thesis intertwines three different fields of study there will be some introductory sections in all the chapters in order to explain the main concepts around which the thesis revolves.\\
In Chapter \ref{CH_BD} we will explain the state of the art in terms of Big Data technologies and we will apply this knowledge to write our code for \emph{Apache Spark} and \emph{Apache Flink} in Chapter \ref{CH_BDCODE}.
A survey of what is Machine Learning and what makes distributed learning so troubling can be found in Chapter \ref{CH_ML}.\\
Then, we will look into how the distributed system to connect the different data centers is designed and we will study its internals  in Chapter \ref{CH_AKKA}.\\
The evaluation of the collaborative cluster, including some eventual improvements, will be carried out in the last chapter.
