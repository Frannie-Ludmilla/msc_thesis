\begin{savequote}[95mm]
	\textquotedblleft \textsf{Hofstadter's Law:} It always takes longer than you expect, \\even when you take into account Hofstadter's Law.\textquotedblright
	\qauthor{Douglas Hofstadter, {\scriptsize\textit{Gödel, Escher, Bach: An Eternal Golden Braid}}}
\end{savequote}
\chapter{Collaborative cluster}\label{CH_AKKA}
\section{Putting all the pieces together}
Now that we know all the required concepts of Big Data, Machine Learning we can put these pieces together to draw a sketch of how our system should be designed in \emph{Akka}.\\
The problem starts with a client (e.g. a doctor) that would like to compare the analysis of a patient with the ones of his/her previous patients in order to proceed with his/her diagnosis: the doctor may have had too few patients so he/she may have too few data to check.
Even if he/she had a big database, still it would be quite likely that rare illnesses never show up there.\\
So he/she may decide to query the nearest hospital dataset. Nevertheless, it is possible that rare illnesses do not show up neither there.
Since he/she wants to rule out that the patient has an uncommon disease, he/she would rather ask to all the available hospitals for a classification of these analysis.
Put in a more technical jargon, the client wants to propagate its query to the federation of cluster nodes (hospitals).\\
Even if hospitals wanted to help the doctor with his/her diagnosis, still they could not allow him/her to look into their datasets because there is private sensitive data. \\
We are therefore tied to run only local computations whose outcomes, however, could be combined to build a global classifier/learner.
Still, in order to propagate the classification task, we should gather all hospitals together inside a logical cluster.\\ 
Every time a node receives a query, the node will be triggered to compute its local outcome and to transmit it to the actor in charge of gathering all the data and of supervising the task (see the majordomo silhouette in fig. \ref{fig:clusterHospitals}).
%Then, we will calculate the global-nearest neighbours and answer to the client's query.\\
\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{images/ClusterExampleHospitals}
	\caption{Sketch of the desired behaviour for our \textsf{CollaborativeCluster}}
	\label{fig:clusterHospitals}
\end{figure}
\\We want to be able to ensure \textit{interoperability}: in our cluster we can launch our own classification algorithm as long as it has been implemented for any of the available frameworks.
Since we have settled for a common format for the intermediate outcome, any node can choose its own favourite platform.\\
We settle for the most straightforward classification algorithm, \textsf{KNN}: locally-computed neighbours will thus be expressed in terms of tuples of distance and class label.\\
In addition to the classification algorithm to use, we need to specify also the metric and the value of ($k$).\\
The local outcomes will be aggregated inside the logical cluster using majority voting as ensemble method and then we will send to the doctor (\textit{outside} the cluster) only the classification label.
%Each hospital that wants to collaborate joins the logical cluster, proves its cooperative will by subscribing to the topic under which the machine learning queries will be published and waits. In order to send its query to the cluster, a client has to know an entrance gate: any cluster node can set a receptionist for incoming connections/requests.\\
%This receptionist will pass the message 

\section{\textsf{CollaborativeCluster}'s privacy proprieties}
Let us survey the privacy proprieties that our \textsf{CollaborativeCluster} architecture ensures and illustrate them with an example:
Doctor Alice sends a query $q_i$ to Hospital $A$ that will in turn broadcast $q_i$ to other two hospitals, $B$ and $C$.\\
Alice knows nothing about the composition of the cluster and its members and it is impossible for her to ascertain anything about the datasets since she will be returned only the predicted classification label as response.\\
$B$ and $C$ perform their computation \textit{locally} and this prevent \textbf{direct leaking} because they never reveal their input data to anyone else and they transmit only tuples that cannot be linked back to particular instances. Moreover, there is no communication between $B$ and $C$ as the intermediate results will be gathered inside Hospital $A$.\\
This responsibility has been dumped on $A$ because by design we select as gatherer the first node to receive the query from \textit{outside} the cluster.\\
Therefore the only participant that can behave \textbf{maliciously} is Hospital $A$ because is the go-between that knows which nodes are in the cluster and who send which data.\\
$A$ is the only one that could try to perform \textbf{background knowledge-based} attacks.\\
Let us assume for example that our instances are constituted in such a way that the instance $i$ inside $q_i$ has distance zero to another instance $j$ if and only if $i$ and $j$ are the same\footnote{This may happen if we send a whole \textsf{Electronic Health Record} \textsf{(EHR)}, see \S \ref{HealthBigData}}.
%(we could think of this type of distance as a \textit{one-to-one function}).\\
Thus, if $A$ founds a zero-valued distance among the tuples inside the intermediate results, $A$ can easily infer which participant has $i$ in its dataset.\\
Of course, if the set of features of $i$ is not unique then we may assume that there are other different instances having distance zero. An example could be a \textsc{EHR} where we strip off everything that was considered \textsf{Personally Identifying Information} (\S \ref{PII}): in that case it is likely to have two or more people with the same -common\footnote{A rare illness would disclose unintentionally the identity of the person}- disease sharing identical values in their analysis.\\
Without having to resort to anonymization techniques and such, we could get rid of these \textbf{background knowledge-based} attacks by delegating the gathering phase to a \textit{trusted third party}.\\
Yet, this trusted third party would become a \textit{single point of failure} in our \textsf{Collaborative\-Cluster} because the client could attempt to send its query to any of its known nodes (more at \S \ref{client_side}) overcoming the connection failure of one node in particular.

\section{Reactive clusters in Akka}
We would like our \textsf{CollaborativeCluster} to accomplish all the requirements so that it could be bestowed with the attribute \textbf{reactive}.\\
A system is defined \textbf{reactive} if it satisfies the following properties:
\begin{itemize}
	\setlength\itemsep{-0.5em}
	\item \textbf{scalable},
	\item \textbf{resilient},
	\item \textbf{responsive} to users
	\item and \textbf{event driven}.
\end{itemize}
When we deal with asynchronous systems we cannot make timing assumptions so it becomes essentially difficult, if not impossible, in case of crashes even something so basic as reaching \emph{consensus}.
Therefore while building a distributed system from scratch, it would take us weeks of coding just for implementing (correctly) message ordering or an eventual failure detection, not to mention consistent shared registers or reliable broadcast.\\
The whole distributed system could rapidly take up thousands of \emph{SLOC}\footnote{Source lines of code} and become a gigantic monolith where in its details dwells not only the devil but also treacherous bugs as well.\\
And we should not forget that we want the aforementioned proprieties of a reactive system to hold true even when we add elements on top of the system core.\\
Nowadays, in the design of complex distributed systems it is favoured an architecture based on \textbf{micro-services} over a \textit{monolithic} one: having components seen as a service allows easier modular deployment and the usage of containers (e.g. Google's \emph{Kubernetes} or Facebook's \emph{Tupperware}) to wrap them in ensures a quicker recovery from faults.
Therefore every added module/service should act as independently as possible, it should be based on message-passing and it should be non-blocking so that it can promptly answers to any event: we would like modules to behave as specialized human workers operating concurrently and capable of rapidly processing external stimuli.\\
This idea in computing does not sound new because it was introduced in the seventies by Carl Hewitt and baptised as the \textquotedblleft \textsc{Actor Model}\textquotedblright: this approach proved its powerfulness to the industry when it was implemented as a main feature of Erlang after more than a decade during which its existence was confined to academical research.\\
An actor is seen as an \textbf{island of consistency} that relates to the other actors only via a \textbf{message-passing} paradigm: a delivered message could trigger a change in the actor private state or could lead it to father other actors.\\
This \textbf{shared-nothing} architecture relinquishes the Actor model from getting entangled with locks or inconsistent internal states while the disposable nature of the actor is perfect when it comes down to fault tolerance and scalability: do you have more requests to satisfy? You can add a new actor for each request, it is as simple as that.\\
Starting from the very beginning, concurrent programming with Actors was paired with functional languages also because things like pattern matching could be implemented easier (in Scala it is a built-in feature). Akka brought to the next level the early Scala Actors offering a fast-evolving framework with many tools that could compete with the maturity and reliability of Erlang Actors.

\subsection[Brief intro to Akka]{Brief intro to Akka, Actors and Automatas}
Akka developers dispelled all doubts about the origin of the name in the FAQ \footnote{Frequently Asked Questions} section of their website\footnote{\url{http://doc.akka.io/docs/akka/current/additional/faq.html}} saying that it comes from a Swedish mountain and, from my point of view, it portrays faithfully how a programmer feels during his/her learning journey: harsh and fatiguing during the ascent, offering a stunning sight at the peak (once you have learnt how to use it) and less tiring on the road downhill.\\
Letting jokes and metaphors aside, let us dive into Akka internals.
\subsection{Actors and Automatas}\label{akka_actors}
An Actor is an object with identity (we can reference it with an \textsf{ActorRef} or via an \textsf{ActorPath}) that has a behaviour (a mutable internal state) and that interacts only through asynchronous message-passing (either via \textsf{!}\footnote{Equivalent to \textsf{tell()}, it follows a \textit{fire-and-forget} rule} or \textsf{?}\footnote{Equivalent to \textsf{ask()}, it involves waiting and carrying the response of the message as a \textsf{Future}}).\\
To specify how an \textsf{Actor} should behave in response to messages we use a \textsf{receive} function.\\
Actors live inside an \textsf{ActorSystem} that during its startup will initialize a \textbf{hierarchical structure} with three actors: the system guardian (\textsf{/system}), the guardian actor (\textsf{/user}) and the root guardian (\textsf{/}) supervising these two.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{images/drwhoakka}
	\caption{Akka Top Level Supervisors}
	\label{fig:drwhoakka}
\end{figure}
\\The supervisor of the root guardian simply set the \textsf{terminated} status to \texttt{true} once all the children of the \textsf{root} have stopped: it is referred to, with a latent \textit{Whovian} connotation, as  \textquotedblleft\textit{the one who walks the bubbles of space-time}\textquotedblright (see fig.\ref{fig:drwhoakka} taken from the online documentation \cite{Akka_online}) because it is out of the \textsf{ActorSystem} \textit{bubble}.\\
Every actor created via a \textsf{system.actorOf()} call is a child of \textsf{user}: an exception to the rule is the \textsf{ClusterClient\-Receptionist} in \S \ref{cluster_node} whose parent is \textsf{system}.\\
Since we do not want to expose the location in memory of our actors and we want to treat equally remote and local actors (\textbf{location transparency}) we use \textsf{ActorRef} references as addressee of messages: an \textsf{ActorRef} address is unique and if we restart its related actor after a crash it will not be valid for the new incarnation.\\
On the other hand we have \textsf{ActorPaths} that are composed by two parts, \textit{authority} and \textit{actual path}: in the first place we specify the \textbf{protocol} to use and the name of the \textsf{ActorSystem} to reach as well as the \textbf{IP address} in case of a remote actor (\textit{authority}) and we append to this stub the path from the root to the desired actor (\textit{actual path}).
\begin{figure}[h!]
	\centering
	\subfloat[Local]{{\includegraphics[width=0.40\textwidth]{images/local-address-annotation}}}%
	\qquad
	\subfloat[Remote]{{\includegraphics[width=0.50\textwidth]{images/remote-address-annotation}}}%
	\caption{\textsf{ActorPaths} in Akka}%
	\label{fig:path}%
\end{figure}
\\A hierarchical structure naturally defines who should supervise who, still it is possible to add lifecycle monitoring (called morbidly \textsf{DeathWatch}) faculties to whichever actor just passing as parameter the ActorRef of the actor to control to \textsf{ActorContext.watch()}.\\
If the actor under surveillance dies, the supervisor will be notified \textit{asynchronously} with a \textsf{Terminated} message: to stop guarding an actor we can use \textsf{ActorContext.unwatch()} but we should keep in mind that even after the call, we may deliver a \textsf{Terminated} message related to that actor if it was stashed in our mailbox.\\
It is possible to kill an actor either ensuring an \textbf{immediate} death with a call to \textsf{context.stop(ActorRef)} or we could send a \textsf{PoisonPill} message to it: in this case the actor, blissfully unaware of its fate\footnote{We could state that Akka is plagiarizing \textit{Chronicle of a Death Foretold} by Gabriel Garc\'{\i}a M\'{a}rquez}, will continue to operate normally and to go through the inbox messages until the delivery of the fatal \textsf{PoisonPill}.\\
Given that our actors are disposable and that they could easily die due to exceptions and crashes, the main rule to follow is:
\begin{framed}
	\centering
	\textquotedblleft \textit{Keep important data near the root, delegate risks to the leaves} \textquotedblright
\end{framed}
\noindent This risk delegation policy favours the employment of ephemeral actors: let us say that a stateful actor $A$ has to write something to a file, it is better to create a stateless actor $B$ supervised by $A$ that works on the low-level file actions that could throw exceptions rather than to have $A$ perform them and implementing complex exception handling and recovery.\\
Another key point while writing Akka Actors is to reduce or, even better, to remove all blocking calls (thus keeping off any \textbf{ask pattern} use) because in distributed systems avoiding all kinds of synchronization issues (\textbf{deadlock}, starvation, \textbf{live-lock}) with always-changing topologies is a demanding challenge.\\
Scalability in Akka is preferred over performance: the asynchronous messaging pattern reinforces vertical scalability whereas the location transparency vouches for the horizontal scalability of our system.\\
The \textbf{Finite State Automata} abstraction goes well with the Actor model: if the actor has only a finite number of possible states it becomes trivial to make it proceed along the designed path for the application.\\
Although the input symbols' set is infinite (there is no bound on the kind of messages we can receive or send), by applying pattern matching we could provide transition functions only when valuable messages get delivered while discarding everything else.\\
There are three possible way to mimic FSA: using boolean variables to change the inner behaviour of the \textsf{receive} function, switching internal states via \textsf{become()} (and \textsf{unbecome()}) or exploiting the \textsf{FSM} mixin heavily based on its Erlang counterpart.
\subsection{Akka Cluster}
John Donne said that \textquotedblleft \textit{No man is an island entire of itself; every man is a piece of the continent, a part of the main}\textquotedblright and this holds true also for our Akka Actors that are well suited for teamwork and that can connect to whichever Cluster they want, provided they know the address of some contact nodes (\textsf{seed nodes}).\\
However, in Akka it is possible to have Clusters composed by only one actor (\textsf{Singleton}) and this is quite usual while starting up the system.\\
Akka Clusters use a \textbf{gossip protocol} for messaging and an \textbf{accrual failure detector} that, when asked whether a node is down or not, instead of answering simply with a \texttt{yes} or \texttt{no} it returns a $\phi$ representing the \textit{likelihood} of that node being down according to its failures' history.\\
Every node is a \textbf{peer}\footnote{In case of an action that requires a leader (e.g. changing the membership state of a node) we simply appoint the oldest node still running in terms of connection time to the Cluster.} knowing the Cluster's full topology and is identified by a \texttt{hostname\-:port:\-uid} tuple (similar to a remote \textsf{ActorPath} address, but with the addition of the \texttt{uid} that uniquely identifies the \textsf{ActorSystem} instance running at that \texttt{hostname:port}).\\
It is possible to enrich our cluster with a Publish/Subscribe messaging pattern by enabling \textsf{DistributedPubSub} extension: every node should start a \textsf{DistributedPubSubMediator} actor that is in charge of keeping track of the Subscribers' \textsf{ActorRef}s and of the routing logic.\\
This mediator will help us with the delivery of messages: we could broadcast our message to all the nodes interested on the topic through a \textsf{DistributedPubSubMediator.Publish()} message or we could opt for a point-to-point transmission via \textsf{Send()}.

\subsection{Akka Streams}
What if the nodes inside our cluster have to exchange files instead of messages?\\
We are dealing with \textit{unreliable} network transmissions but we want all the data to be successfully transferred: this problem should ring a bell because it is the reason why we employed \textbf{TCP} in the first place.\\
Unfortunately, we have more requirements to add on top of that: the necessity of \textbf{non-blocking} calls (in order to shift towards a fully asynchronous system) and the wish to decouple the \textit{Consumer} processing speed (usually slower) from the \textit{Producer} production pace.\\
Whereas the \textquotedblleft\textit{fast consumer, slow producer}\textquotedblright\  issue does not bring about too many complications, the \textquotedblleft\textit{fast producer, slow consumer}\textquotedblright\  problem can easily degrade performance on both sides because a very sluggish consumer will end up with a buffer overflow and the \textit{producer} (or someone else on his behalf) will have to resend all messages that were not \textit{ACK}ed, wasting bandwidth and computing resources (nevertheless, please note that this is essentially the TCP policy).\\
Akka Streams' \textbf{back-pressure} mechanism is referred to as \textbf{dynamic push-pull mode}: we signal through a \textsf{Request(}\textit{no. of items}\textsf{)} how many elements the consumer can safely process in a given time frame.\\
If the \textit{consumer} is \textit{fast}, it will request items as soon as the \textit{producer} emits them (\textit{push mode}) otherwise, if it is \textit{slow}, the \textit{producer} will have to slow down its production rate to match the \textit{consumer} processing speed (\textit{pull mode}).\\
In order to transfer a file, a stream has to be \textsf{materialized} therefore we will specify the file as a \textsf{Source}, its content will represent the \textsf{Flow} and the intended receiver will be our \textsf{Sink}. Please note that the materialization will not occur if the \textsf{Flow} has a loose end (e.g. no Sink or no Source) because it can run only if there is something \textit{sealing} both extremities.\\
This stream with \textsf{Source}, \textsf{Sink} and \textsf{Flow} abstractions is called a \textsf{RunnableGraph} and, as any graph worthy of its name, can be freely sketched out and modified according to our demands: let us explain this powerful feature by looking at its employment inside our application.\\
We will dive into the details later in \S \ref{entrails}, so far it is enough to know that we have many \textit{clients} that must send their intermediate results to a \textit{server} by materializing their streams on top of TCP.\\
Due to the definition of Akka TCP Streams, we must employ an \textsf{IncomingConnection} as the \textsf{Source} and an \textsf{OutgoingConnection} as the \textsf{Sink} while the content will be emitted as a \textsf{Flow} of \texttt{ByteString}.\\
At the \textit{client} side it is straightforward: we create our emitting \textsf{Source} by calling \textsf{FileIO.fromFile(inputFile)}\footnote{This will spill the file content as if it was a stream} and we attach it to our TCP socket via \textsf{Tcp().outgoingConnection(} \textit{address, port}\textsf{)}.\\
The server is listening to any \textsf{Tcp.IncomingConnection} but the related graph of a full-duplex TCP socket is already closed because the client's \textsf{Sink} is the server's \textsf{Source} and vice-versa. Therefore, we should make a copy of the Flow and we should guide its content towards a file working as another \textsf{Sink} while the original \textsf{Flow} follows its usual path.\\
The documentation is still a little obscure because it is in its early phase but once a programmer get the grasp of Akka Streams it becomes easy to build even complex graphs to satisfy his/her needs.

\section{A bird's eye view of \textsf{CollaborativeCluster}}
We will use a \textit{top-bottom} approach to explain how the cluster works and we will designate all the parts of the distributed system with the same names used in the code written with a \textsf{sans serif} font.
Our starting point is a client that wants to spread its classification query to as many nodes as possible knowing that there is a logical cluster (modelled as an \textsf{ActorSystem}) called \textsf{CollaborativeCluster} where they all gather (the aforesaid federation).\\
We do not care about these nodes' architectures (single server, cluster, grid...) because the \textsf{CollaborativeCluster} is just a service that will interoperate with the others that are already dwelling in the node: it seemed better to conceive from the start this distributed system as a union of micro-services rather than as a monolith program.\\
Each node is called \textsf{ClusterNode} and uses two gates to communicate: a \textsf{ClusterClient\-Receptionist} that allows \textit{incoming requests} from \textit{outside} the logical cluster and a \textsf{Mediator} that takes care of both \textit{incoming and outgoing} requests \textit{inside} the logical cluster.\\
The client will send its query to the remote address of a \textsf{ClusterClient\-Receptionist} that will deliver the message to the \textsf{ClusterNode} and that, in turn, will broadcast it to all the reachable nodes in the cluster via its \textsf{Mediator}.\\
In Akka a cluster follows a publish/subscribe paradigm: the query message is seen as an \textit{event} associated to a topic (\textsf{MLTask} in our case) and the \textsf{ClusterNode} can be both a publisher and a subscriber\footnote{If it wants to be notified about published data on that topic} that delegates the burden of the \textit{Event Notification Service} on the shoulders of the \textsf{Mediator}, that acts as a \textit{broker}.\\
That is why we talk about \textit{cooperation}: every \textsf{ClusterNode} voluntarily decides whether to help or not by subscribing or revoking its subscription to the topic \textsf{MLTask}.\\
Unfortunately our client does not belong to the cluster and this means that it is not aware of what is happening there so we have to think about how to re-route the collected information to it.

\section[Internals of \textsf{CollaborativeCluster}]{Internals of \textsf{CollaborativeCluster}}\label{entrails}
\subsection{Client side}\label{client_side}
We start by creating an \textit{ActorSystem} whose main role is played by a \textsf{RequestActor} that instantiates a (logical) connection to the Cluster by means of a \textsf{ClusterClient}.\\
We need to use a \textsf{ClusterClient} instance if we want to be able to resolve our remote actors addresses related to the other ActorSystem, \textsf{CollaborativeCluster} as it works as an intermediary between them.\\
The \textsf{RequestActor} sends the details of the classification task inside an \textsf{ExternalRequest} message to the \textsf{ClusterClient} that will deliver it to the known \textsf{ClusterClient\-Receptionist} address\footnote{We are not bound to pass to the \textsf{ClusterClient} only one address: we could have a set of addresses such that if the \textsf{ClusterClient} cannot connect to the first one, it automatically try with the others}.\\
\subsection{\textsf{ClusterNode} side}\label{cluster_node}
When a \textsf{ClusterNode} receives an \textsf{ExternalRequest} message from its associated \textsf{ClusterClient\-Receptionist} we must ensure two things:
\begin{enumerate}
	\item That there is an \textit{entity} (designed as \textsf{MajordomoActor}) keeping track of the status of the classification task happening \textit{concurrently} in the cluster and that remembers the address of the sender\footnote{Inside the \textsf{CollaborativeCluster}, the \textit{actual} sender of the message is the \textsf{ClusterClient} and not the \textsf{RequestActor} that created it, so the stored address is that of the former.}
	\item and that not only the delivered message but also the address of the entity in charge of it will be broadcast to every \textsf{ClusterNode} that is  currently a subscriber to that topic.
\end{enumerate}
The \textsf{MajordomoActor} entity is required because we want to send just the classification label to the client and to relieve it from exchanging messages to the other \textsf{ClusterNode}s.
We do not want our communication speed to be bounded to the bandwidth of the client and to its computational power because it may be a \textit{slow producer} or a \textit{slow consumer} and this would harness performance even if most of the communication will be asynchronous.\\
As soon as a \textsf{ClusterNode} receives a \textsf{ClusterRequest} (composed by the contents of the \textsf{ExternalRequest} message and by the \textsf{MajordomoActor} reference) it will delegate everything related to the computation and the transmission of the classification results to an entity called \textsf{BackendNode}.\\
It is worthy to stress out that since the \textsf{ClusterRequest} message was broadcast, also the \textsf{ClusterNode} that sent it out will deliver it too: therefore in that \textsf{ClusterNode} both the \textsf{MajordomoActor} and the \textsf{BackendNode} entity will be instantiated.
\subsubsection{MajordomoActor}\label{majordomoactor}
The \textsf{MajordomoActor} role is to keep track of the \textsf{BackendNode}\textit{s} available for the task, to receive their computations' outcomes and, when every result is collected or if the \textsf{Timeout} timer has expired\footnote{We do not want to wait forever the result from a \textit{remote} \textsf{BackendNode} that may have crashed}, to perform the last step of the classification by taking the top $k$ neighbours retrieved from all over the cluster and by choosing the label via majority vote of those neighbours.\\
These steps were implemented as two \textit{internal states} of \textsf{MajordomoActor} acting as a \emph{Finite State Machine} (FSM): a \textsf{collectTasks} and a \textsf{processing\-GatheredData} phase.\\
In the former phase we start by creating a \textsf{TransferServer} that will stores in a temporary folder the results coming from the \textsf{BackendNode}s via \textit{Akka Streams} on top of a TCP connection and by storing the addresses of all those which sent a \textsf{FrameworkFound} message\footnote{A \textsf{BackendNode} needs to send this message as a positive ACK because even if during its initialization everything went all right, this do not ensures that the  classification can be carried out through the Big Data frameworks present in the node}.\\
Every address of the sender of a \textsf{FrameworkFound} message will be stored in the list of \textsf{remoteHelpers} that will be updated either whenever a \textsf{BackendNode} sends an \textsf{AllDone} or a \textsf{FailedJob} message.\\
When everybody has replied, we will prompt a \textsf{Gathere\-rActor} to compute the majority vote over the $k$ nearest neighbours that will be retrieved from the files in the temporary folder and we trigger the transition to the \textsf{processingGatheredData} state.\\
During this latter phase, the \textsf{Majordomo\-Actor} waits for the \textsf{Gatherer\-Actor}'s response (no matter if it is a classification label or an error), forwards it to the \textsf{ClusterClient} and safely shuts down itself and its subordinate actors (\textsf{Transfer\-Server} and \textsf{Gatherer\-Actor}).
\subsubsection{BackendNode}
A \textsf{BackendNode} has to:
\begin{enumerate}
	%\setlength\itemsep{-0.2em}
	\item check that the environment is correctly configured and that Big Data frameworks are available;
	\item computing an intermediate outcome for the $k$ Nearest Neighbour classification task;
	\item send this data to the \textsf{Majordomo\-Actor} in charge of that query via a \textsf{TransferClient}.
\end{enumerate}
We want to check the availability not only of the frameworks (\emph{Apache Spark} and \emph{Apache Flink}) but also of the \textit{jobs} crafted for them.\\
During the initialization of \textsf{BDFWatcherActor}, the actor in charge of monitoring those jobs, we pass also all those configuration values required for a correct execution of our computation (where we can find the datasets to survey, in which folder intermediate results should be stored, etc...). \\
The FSM related to a \textsf{BackendNode} entity consist of four states:
\begin{description}[labelindent=0.5cm]
	\item[\textbf{Start}] where we retrieve all the Machine Learning task details and we pass them to a newly created \textsf{BDFWatcherActor};
	\item[\textbf{Setup}] where we wait for the response of this latter over the availability of the necessary tools for the computation (\textsf{FrameworkFound} or \textsf{NoFrameworkDetected});
	\item[\textbf{PerformTask}] where we wait until the end of the execution of the KNN algorithm and we hand over the temporary folder path containing the results to the \textsf{TransferClient} actor; 
	\item[\textbf{Transmission}] where we send either an \textsf{AllDone} or a \textsf{FailedJob} message to the \textsf{Majordomo\-Actor} to positively or negatively acknowledge the termination of the \textsf{TransferClient} actor. 
\end{description}
The intermediate results sent via Akka Streams by the \textsf{TransferClient} actor are formatted as $k$ comma-separated values of type $<distance,\ label>$: this restriction is necessary to guarantee the privacy of the data contained in the harvested datasets.

\begin{figure}[h]
	\includegraphics[width=\textwidth]{images/BackNode}
	\caption{BackendNode and its actors}\label{fig:BN}
\end{figure}

\subsection{Notes on Akka Actors deployment}
The system is designed to make limited use of actors with changing state variables and to delegate all those actions that can throw errors and exceptions to the \textit{bottom} of the actors' hierarchy (see \S \ref{akka_actors}).\\
Whenever something goes awry in an actor, the actor itself or its supervisor (if the actor \textit{died}) handles the problem by sending error messages (e.g. \textsf{FailedJob}) that will trigger either a transition to a final state or will update the data associated to that state.\\ The latter solution is preferred if the fault does not hinder the correct execution of another actor: for example, if one of the \textsf{BackendNode} in the cluster cannot perform the computation, we will remove it from the list of \textsf{remoteHelpers} inside the \textsf{Majordomo\-Actor} but we will not shut down the \textsf{Majordomo\-Actor} because there might be other  \textsf{BackendNode}s running our query or checking their environment.\\
Since we force each actor to supervise its own \textit{subordinate} actors and we lead the system or the faulty component to a graceful exit when a substantial error occurs, the \textsf{CollaborativeCluster} system is ensured to be \textbf{fail-safe}.\\
%Every \textsf{BackendNode} and \textsf{Majordomo\-Actor} entity supervises the life-cycle of its instantiated \textit{subordinate} actors and any failure in these two entities does not need to be reported to their \textit{parent} actor because it does not compromise the stability of the \textsf{CollaborativeCluster}: the system is ensured to be \textbf{fail-safe}.\\
%and it is supervised by its parent actor 
A detailed sketch of the \textsf{CollaborativeCluster} system can be seen in fig. \ref{fig:all_DS}
\begin{sidewaysfigure}
	\centering
 	%\includegraphics[width=\textwidth]{}
 	\includegraphics[width=\textwidth]{images/ALL_3_version}
 	\captionsetup{justification=centering, labelsep=newline}
 	\caption[Internals and message exchange blueprints in \textsf{Collaborative\-Cluster} ]{Internals and message exchange blueprints between\\ \textsf{Collaborative\-Cluster} (whose actors have been highlighted with blue background)\\ and the Client's \textsf{RequestActor} (grey background).\\Dashed lines represent \textit{transition functions} while continuous lines designate \textit{messages}.}\label{fig:all_DS}
\end{sidewaysfigure}