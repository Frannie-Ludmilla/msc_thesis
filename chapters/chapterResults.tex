\begin{savequote}[60mm]
	\textquotedblleft Every thing's got a moral,\\if only you can find it.\textquotedblright
	\qauthor{\scriptsize Lewis Carroll, \emph{Alice in the wonderland}}
\end{savequote}

\chapter{Results}\label{conclusion_chapter}
What defines whether our \textsf{CollaborativeCluster} approach pays off or not?\\
The ultimate aim of its development is to help classification tasks\footnote{But we could easily extend it to other machine learning tasks} without giving away precious details about the private data inside the nodes of the cluster, thus our first concern should be to determine if there is any improvement in terms of accuracy over a classification computed on a single node.\\
Secondly, we are employing Big Data frameworks so it would be nice to sketch out how the execution time grows when we increase the input size: since the Thyroid dataset is nowhere near what we call Big Data, we will focus on the results achieved with the SUSY dataset (whose size is about 2.4 GB).\\
Lastly, we will look at how we could mitigate the weaknesses exposed by our analysis.

\section{Accuracy}\label{conclusion_accuracy}
Reality is messy and biased.\\
The previous statement is not an expression of teenage angst: it happens frequently that our training sets are biased towards a class and that other datasets on the same topic have more instances than ours.\\ 
We will put down a dataset as \textbf{biased} when its internal composition is skewed whereas we will say that two datasets are \textbf{unbalanced} if there is a remarkable difference in size between them.\\
Let us assume that our cluster is composed by two nodes and that the union of their datasets is equal to the largest splits we have considered in \S \ref{survey_ML_datasets}: if we join the information of the two nodes, we should attain therefore a similar (possibly the same) accuracy level of a single node computation over the whole original dataset, no matter how the data is distributed.\\
The different combinations that we can encounter are four:
\begin{enumerate}
	\setlength\itemsep{-0.2em}
	\item \textbf{Balanced and Biased}: the two datasets have the same size but inside each dataset a class has more instances than the other ones;
	\item \textbf{Balanced and Unbiased}: we have the same size and a similar number of instances for all the classes;
	\item \textbf{Unbalanced and Biased}: the two datasets have different sizes and their internal composition is skewed towards a class;
	\item \textbf{Unbalanced and Unbiased}: we have different sizes but the number of instances for each class is mostly equivalent.
\end{enumerate}
Since almost the entirety of the Thyroid dataset is composed by instances of only one class, we will contemplate for it only the first and the third case as there is no way of getting rid of the bias without causing substantial changes in the dataset (a more detailed analysis will be carried out in \S \ref{improving_thyroid}).\\
We will start by examining our tests on the SUSY dataset because this allows us to examine all the four possibilities.
\subsection{SUSY}
%\subsubsection{Balanced and Biased}
The skewed datasets were created synthetically from the \textsf{Split90} dataset: the first split was populated by selecting randomly as many instances of the two classes as we needed and then the second split was achieved from the difference between the first and the whole dataset.\\
We can observe that by putting together the outcomes of the two splits we achieve the same level of accuracy that we would have had with a centralized execution harvesting the whole dataset. The only exception is the \textsf{Second Split} of the case \textsf{Balanced} and \textsf{Biased} where our focus on a smaller dataset with more instances of class $1$ is rewarded with a more accurate classification of these \textsf{signal} examples.\\
Unfortunately, in order to make perfectly \textbf{unbalanced} and \textbf{unbiased} the SUSY dataset we needed to remove some instances and for this reason the accuracy level is not the same as the one we would have had with the original dataset.\\
Nevertheless, it still holds true that the distributed execution achieves the same accuracy (as well as the same averaged \textit{Precision}, \textit{Recall} and $F_1$ \textit{score}) as the centralized one computed over the union of the two splits (this was highlighted in grey in \ref{tab:susy_unbalunbia}).\\
We have everywhere else an increase on the correct classifications of a class when the dataset is slightly biased in his favour.\\
If we had paid attention only to the \textsf{average accuracy}, we would have missed that we have more instances correctly classified belonging to label $0$ than those of label $1$ (according to a trend that we highlighted in \S \ref{susy_whole_ml}) and that the bias strongly influences the per-class accuracy level.\\
\begin{table}[h]
	\centering
		\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{lrccccccccc}
			\toprule
			Data skewness & Training set & \specialcell{accuracy\\wrt\\ label $0$} & \specialcell{accuracy\\wrt\\ label $1$} & \specialcell{accuracy} & \specialcell{total\\ instances} & \specialcell{class 0\\ instances\\ (\%)} & \specialcell{class 1\\ instances\\ (\%)} & Precision & Recall & $F_1$ score\\
			\midrule
			{} &\textsf{Whole dataset} & 0.91 & 0.62 & 0.76 & 4500000 & 54.25 & 45.75 & 0.71 & 0.84 & 0.62\\
			\midrule
			\multirow{3}{*}{\textsf{Balanced Biased}} &\textsf{Two nodes} & 0.91 & 0.62 & 0.76 & 4500000 & 54.25 & 45.75 & 0.71 & 0.84 & 0.62\\
			{} & \textsf{SN: First split} & 0.98 & 0.54  & 0.76 &2250000 & 72.5 & 27.5 & 0.70 & 0.96 & 0.55\\
			{} & \textsf{SN: Second split} & 0.79 & 0.83 & 0.81 &2250000 & 36.02 & 63.98 & 0.79 & 0.74 & 0.83\\
			\midrule
			\multirow{3}{*}{\textsf{Balanced Unbiased}} & \textsf{Two nodes} & 0.91 & 0.62 & 0.76 & 4500000 & 54.25 & 45.75& 0.71 & 0.84 & 0.62\\
			{} & \textsf{SN: First split} & 0.87 & 0.66 & 0.77 &2250000 & 54.25 & 45.75& 0.73 &	0.8 & 0.67\\
			{} & \textsf{SN: Second split} & 0.86 & 0.69 & 0.77 &2250000 & 54.25 & 45.75 & 0.73 & 0.78 & 0.69\\
			\midrule
			\multirow{3}{*}{\textsf{Unbalanced Biased}} & \textsf{Two nodes} & 0.91 & 0.62 & 0.76 & 4500000 & 54.25 & 45.75& 0.71 & 0.84 & 0.62\\
			{} & \textsf{SN: Split 60} & 0.96 & 0.54 & 0.75 & 2688398 & 70.30 & 29.70& 0.69 & 0.92 & 0.55\\
			{} & \textsf{SN: Split 40} & 0.67 & 0.78 & 0.72 & 1811602 & 30.45 & 69.55& 0.70 & 0.63 & 0.79\\
			\midrule
			\multirow{4}{*}{\textsf{Unbalanced Unbiased}} & \textsf{Two nodes} & 0.89 & 0.71 & 0.81 & 4116796 & 50 & 50 & 0.75 &
			0.83 & 0.69\\
			{} & \textsf{SN: Split 60} & 0.82 & 0.73 & 0.78 & 2470078 & 50 & 50 & 0.75 & 0.79 &	0.71\\
			{} & \textsf{SN: Split 40} & 0.86 & 0.71 & 0.79 & 1646718 & 50 & 50& 0.75 & 0.76 & 0.74\\
			\rowcolor{lightgray} 
			{} & \textsf{SN: whole dataset} & 0.89 & 0.71 & 0.81 & 4116796 & 50 & 50& 0.75 & 0.83 & 0.69\\
			\bottomrule
		\end{tabular}		
	\end{adjustbox}
	\caption{SUSY: Accuracy results}\label{tab:susy_unbalunbia}
\end{table}
\\This can be proved also by looking at the confusion matrix\footnote{Note that the disposition of \textit{Predicted} and \textit{Actual} instances inside the \textsf{matplotlib} confusion matrix is inverted with respect to the usual confusion matrix shown in \ref{tab:conf_Matr_idea} in Chapter \ref{CH_ML}.} and the per-class values of \textit{Precision}, \textit{Recall} and $F_1$ \textit{score} in tab. \ref{tab:conf_Matr_SUSY}.
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=0.85\textwidth}
\begin{tabular}{llcclllll}
	&                                                                                              & \multicolumn{2}{c}{\textbf{Predicted}}                                                                                                                                                  &                       &                                                                                             &                                         &                                      &                                       \\ \cline{3-4} \cline{7-9} 
	& \multicolumn{1}{l|}{}                                                                        & \multicolumn{1}{c|}{\textit{\begin{tabular}[c]{@{}c@{}}Class 0\\ (Background)\end{tabular}}} & \multicolumn{1}{c|}{\textit{\begin{tabular}[c]{@{}c@{}}Class 1\\ (Signal)\end{tabular}}} & \textit{}             & \multicolumn{1}{l|}{}                                                                       & \multicolumn{1}{l|}{\textbf{Precision}} & \multicolumn{1}{l|}{\textbf{Recall}} & \multicolumn{1}{l|}{\textbf{$F_1$-score}} \\ \cline{2-4} \cline{6-9} 
	\multicolumn{1}{c|}{\multirow{2}{*}{\textbf{Actual}}} & \multicolumn{1}{r|}{\textit{\begin{tabular}[c]{@{}r@{}}Class 0\\ (Background)\end{tabular}}} & \multicolumn{1}{c|}{53}                                                                      & \multicolumn{1}{c|}{5}                                                                   & \multicolumn{1}{c|}{} & \multicolumn{1}{r|}{\textit{\begin{tabular}[c]{@{}r@{}}Class 0\\ (Background)\end{tabular}}} & \multicolumn{1}{c|}{0.77}               & \multicolumn{1}{c|}{0.91}            & \multicolumn{1}{c|}{0.83}             \\ \cline{2-4} \cline{6-9} 
	\multicolumn{1}{c|}{}                                 & \multicolumn{1}{r|}{\textit{\begin{tabular}[c]{@{}r@{}}Class 1\\ (Signal)\end{tabular}}}     & \multicolumn{1}{c|}{16}                                                                      & \multicolumn{1}{c|}{26}                                                                  & \multicolumn{1}{c|}{} & \multicolumn{1}{r|}{\textit{\begin{tabular}[c]{@{}r@{}}Class 1\\ (Signal)\end{tabular}}}    & \multicolumn{1}{c|}{0.83}               & \multicolumn{1}{c|}{0.62}            & \multicolumn{1}{c|}{0.71}             \\ \cline{2-4} \cline{6-9} 
\end{tabular}
\end{adjustbox}
    \captionsetup{justification=centering}
	\caption{SUSY's \textsf{Whole dataset}: Confusion Matrix and \textit{Precision}, \textit{Recall} and $F_1$ \textit{score}}\label{tab:conf_Matr_SUSY}
\end{table}

\subsection{Thyroid}
The UCI Thyroid dataset is so strongly biased that it is not possible to create unbiased splits without producing significant changes: nonetheless, we will explore some advanced techniques to solve this problem in \S \ref{improving_thyroid}.\\
During the assembling of our synthetic splits derived from the \textsf{Split 90} we discovered the presence of duplicates in our dataset.
\emph{Apache Spark} shell was used to generate the splits: in the first place we randomly pick up instances (\textsf{takeSample()} may have duplicates in its outcome) for the first split.\\
Then, we obtain the second split by calling \textsf{subtract(}{\small \textit{Split1}}\textsf{)} from the whole dataset, the duplicates in the resulting dataset were removed: thus, the union of the two splits has a smaller size than the original dataset.\\
Fortunately, since all duplicates belonged to the predominant class ({\small\textit{Class 3}}), their removal seems to have not caused any interference with the classification.\\
For multi-class classification we would need to look at the confusion matrix and calculate our \textit{F-measure}, \textit{Precision} and \textit{Recall} over each class (see tab. \ref{tab:conf_Matr_thy}).
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=0.9\textwidth}
		\begin{tabular}{clcccllccc}
			\multicolumn{1}{l}{}                &                                       & \multicolumn{3}{c}{\textbf{Predicted}}                                                                                &  &                                       & \multicolumn{1}{l}{}                   & \multicolumn{1}{l}{}                & \multicolumn{1}{l}{}                 \\
			\multicolumn{1}{l}{}                &                                       & \multicolumn{1}{l}{\textit{Class 1}} & \multicolumn{1}{l}{\textit{Class 2}} & \multicolumn{1}{l}{\textit{Class 3}} &  &                                       & \multicolumn{1}{l}{\textbf{Precision}} & \multicolumn{1}{l}{\textbf{Recall}} & \multicolumn{1}{l}{\textbf{F-score}} \\ \cline{3-5} \cline{8-10} 
			\multirow{3}{*}{\textbf{Actual}} & \multicolumn{1}{l|}{\textit{Class 1}} & \multicolumn{1}{c|}{11}              & \multicolumn{1}{c|}{3}               & \multicolumn{1}{c|}{4}               &  & \multicolumn{1}{l|}{\textit{Class 1}} & \multicolumn{1}{c|}{0.92}              & \multicolumn{1}{c|}{0.61}           & \multicolumn{1}{c|}{0.73}            \\ \cline{3-5} \cline{8-10} 
			& \multicolumn{1}{l|}{\textit{Class 2}} & \multicolumn{1}{c|}{1}               & \multicolumn{1}{c|}{7}               & \multicolumn{1}{c|}{19}              &  & \multicolumn{1}{l|}{\textit{Class 2}} & \multicolumn{1}{c|}{0.64}              & \multicolumn{1}{c|}{0.26}           & \multicolumn{1}{c|}{0.37}            \\ \cline{3-5} \cline{8-10} 
			& \multicolumn{1}{l|}{\textit{Class 3}} & \multicolumn{1}{c|}{0}               & \multicolumn{1}{c|}{1}               & \multicolumn{1}{c|}{54}              &  & \multicolumn{1}{l|}{\textit{Class 3}} & \multicolumn{1}{c|}{0.70}              & \multicolumn{1}{c|}{0.98}           & \multicolumn{1}{c|}{0.82}            \\ \cline{3-5} \cline{8-10} 
		\end{tabular}
	\end{adjustbox}
	\captionsetup{justification=centering}
	\caption{Confusion Matrix for centralized execution over Thyroid's \textsf{Whole Dataset}}\label{tab:conf_Matr_thy}
\end{table}
\\Although this detailed view can be useful to know which are the pitfalls of our classifier, we will present in tab \ref{tab:thyroid_unbalunbia} their average \textit{Precision}, \textit{Recall} and $F$ \textit{score} weighted by their \textbf{support}\footnote{Where \textbf{support} means the true positive values for the classes, see '\textbf{weighted}' in \url{http://scikit-learn.org/stable/modules/generated/sklearn.metrics.precision_recall_fscore_support.html}} in order to provide a global outlook.
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{rrcccccccccc}
			\toprule
			Data skewness & Training set & \specialcell{accuracy \\wrt\\ label $1$} & \specialcell{accuracy \\wrt\\ label $2$}& \specialcell{accuracy \\wrt\\ label $3$} & \specialcell{accuracy} & \specialcell{training\\ instances\\ class 1}  & \specialcell{training\\ instances\\ class 2} & \specialcell{training\\ instances\\ class 3} & Precision & Recall & $F$ score\\
			\midrule
			{} &\textsf{Whole dataset} & 0.61 & 0.25 & 0.98 & 0.72 & 2 \% & 5 \% & 93 \% & 0.72 & 0.72 & 0.68\\
			\midrule
			\multirow{3}{*}{\textsf{Balanced Biased}} &\textsf{Two nodes} & 0.61 & 0.25 & 0.98 & 0.72 & 2 \%& 5 \% & 93 \%&  0.72 & 0.72 & 0.68\\
			{} & \textsf{SN: First split} & 0.55 & 0.18 & 0.98 & 0.69 & 2 \%& 5 \% & 93 \% & 0.68 & 0.69 & 0.64\\
			{} & \textsf{SN: Second split} & 0.11 & 0 & 0.78 & 0.45 & 2 \%& 5 \% & 93 \%& 0.31 & 0.45 & 0.37\\
			\midrule
			\multirow{3}{*}{\textsf{Unbalanced Biased}} & \textsf{Two nodes} & 0.61 & 0.25 & 0.98 & 0.72 & 2 \% & 5 \% & 93 \%&  0.72 & 0.72 & 0.68\\
			{} & \textsf{SN: Split 60} & 0.55 & 0.18 & 0.98 & 0.69 & 1 \% & 4 \% & 95 \% & 0.68 & 0.69 & 0.64\\
			{} & \textsf{SN: Split 40} & 0.33 & 0 & 0.85 & 0.45 & 3 \% & 8 \% & 89 \% & 0.38 & 0.53 & 0.44\\
			\bottomrule
		\end{tabular}		
	\end{adjustbox}
	\caption{Thyroid: \textit{Accuracy} results and averaged \textit{Precision}, \textit{Recall} and $F_1$ \textit{score}}\label{tab:thyroid_unbalunbia}
\end{table}

\section{Performance analysis}
\subsection{Apache Flink vs Apache Spark}
We said in \S \ref{BD_PARADIGMS} that \emph{Apache Spark} and \emph{Apache Flink} use different approaches for both stream (\textbf{micro-batch} vs \textbf{pure streaming}) and batch processing purposes: indeed, \emph{Apache Flink} considers batch jobs as if they were a special case of stream processing with \textit{bounded streams} continuously pipelining data to the next phase.\\
Given the difference in their workings, it is not surprising the divide in terms of performance between the two frameworks (fig. \ref{fig:spark_flink_runtime}) even if their code was specifically written such that it follows the same logic and it does not involve any special trick or function that the other framework does not have.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.70\textwidth]{images/Performance_Flink_Spark}
	\vspace{-30pt}
	\captionsetup{justification=centering,labelsep=newline}
	\caption[Comparison between \emph{Apache Flink} and \emph{Apache Spark} performance]{Comparison between \emph{Apache Flink} and \emph{Apache Spark} performance \\evaluated over 100 queries}\label{fig:spark_flink_runtime}
\end{figure}
\\Still, we should not blame only the different handling of batch data because processing should be performed \textit{utterly} in memory with just a few data spilled to disk once in a while. Therefore there \textit{\textbf{must be}} something in the memory management of the frameworks that determines this performance gap.\\
\emph{Apache Spark} trusts fully the \textbf{JVM garbage collection} and it uses by default Java serialization (although \emph{Kryo} serialization could be enabled too): this means that we are burdened by the overhead given by object headers (16 bytes), boxed primitives (\textsf{double} to \textsf{Double}, \textsf{int} to \textsf{Integer}) and by wrapper objects surrounding instances of common collections (\textsf{List}\textit{s}, \textsf{Map}\textit{s}) (for more information refer to the section \textit{Tuning} in \emph{Apache Spark} Documentation \cite{ApacheSparkGuide}).\\
The tuples of our datasets will be stored in the heap that, given the overhead due to the bulky serialization and the garbage collection, is likely to get filled up very quickly.\\
\emph{Apache Flink} creators decided to implement their own \textsf{MemoryManager} and serialization framework \cite{ApacheFlinkInternals}.\\
Instead of clogging the heap by repeatedly pushing and removing every data object, each Flink worker, during its initialization, requires the allocation of a large contiguous percentage of it\footnote{By default \emph{Flink} asks for the 70\% of the heap but we can tweak the percentage} where objects could be stored and removed without ever bothering the garbage collector. This chunk of memory is composed by fixed-size\footnote{The default value is 32KB} blocks called \textsf{MemorySegments} and it acts as a large buffer of binary data: fragmentation should not represent an issue in this \textit{data buffer} because we only need to save a large amount of data having the same size.\\
Since both Spark's \textsf{RDD}s and Flink's \textsf{DataSet}s store only serialized data, \emph{Flink} serialization approach operates at byte level on \textsf{MemorySegments} and it is more efficient over any type of data\footnote{internally represented with a \textsf{TypeInformation}} and collection because ot uses the insights coming from the compiler and it exploits the peculiar memory management of \emph{Apache Flink}.\\
Let us see what this implies in terms of memory usage for structures like \textsf{List}\textit{s} or \textsf{Vector}\textit{s}: we can store in a separate memory region a \textit{$<$sort key, value$>$} pair for each object where the key has a fixed size and it identifies the object depending on its data type whereas the value is the pointer into the aforementioned \textit{data buffer}.\\
Since with JVM memory management objects could have been scattered all over the heap, we would have needed a wrapper for the sequence of items and for each of them we should have put a pointer to the following object.\\
The strength of Flink approach can be acknowledged when we need to sort the tuples residing inside our datasets because instead of sorting (and moving) the actual data inside our data buffer, we can simply order our \textit{$<$key, value$>$} tuples: why sorting the whole data when you can sort the pointers! (fig. \ref{fig:sort_buffer})
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{images/sorting-flink}
	\vspace{-10pt}
	\caption{\emph{Apache Flink} sort buffer in action}\label{fig:sort_buffer}
\end{figure}
\\All things considered, even if in the configuration \emph{Apache Spark} and \emph{Flink} were told to use the same amount of memory per worker and the same degree of parallelism\footnote{It was set to 4 following the recommendation to use the number of the physical CPU cores}, \emph{Flink}'s higher throughput was to be expected because of its \textit{off-the-shelf} optimization of memory management.\\
For this reason, all the following tests related to the \textsf{CollaborativeCluster} were executed by setting \emph{Apache Flink} as the default choice.

\subsection{Execution time boundaries}
Let us start by saying that our implementations of \textsf{KNN} do not make use of any pre-processing step and this means that for every query we re-read and survey the whole training set: therefore the execution time will be at least \textit{linear} to the size of the dataset.\\
We are using frameworks that parallelize for us the computation, yet this is done so \textquotedblleft \textit{automagically}\textquotedblright  that it is harsh to determine the fraction of the program (denoted as $\alpha$ in eq. \ref{eq:amdahl}) following a serial execution: thus we should think of an alternative to \textbf{Amdahl law} (eq. \ref{eq:amdahl}) for estimating our \textbf{speedup}.
\begin{equation}\label{eq:amdahl}
	S_{Amdahl} = \frac{T_{s}}{T_{p}} = \frac{T_{s}}{\alpha T_{s} + (1- \alpha)\frac{T_{s}}{p} } = \frac{1}{\alpha + \frac{(1 - \alpha)}{p}}
\end{equation}
In table \ref{tab:flink_speed} we can see how the execution time changes when we increase the degree of parallelism by calling \textsf{env.setParallelism()} while leaving the input unchanged\footnote{In this case it is the dataset with 4500000 instances}.
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=0.45\textwidth}
		\begin{tabular}{rc}
			\toprule
			\specialcell{no. parallel instances} & average time\\
			\midrule
			\specialcell{(sequential) 0} & 79.47 s\\
			1 & 82.72 s\\
			2 & 44.88 s\\
			3 & 31.99 s\\
			4 & 26.79 s\\
			%\midrule
			%82.72 s & 44.88 s & 31.99 s & 26.79 s\\
			\bottomrule
		\end{tabular}		
	\end{adjustbox}
	\caption{Average running time: fully sequential Vs \emph{Apache Flink}}\label{tab:flink_speed}
\end{table}
\\\noindent The results are compared to the performance of a sequential Scala program that parses line by line the training set, computes the distances, stores the smallest $k$ ones on a priority queue (it is the centralized version of the code explained in \S \ref{spark_susy}) and finally performs a majority voting.\\
\begin{comment}
\begin{framed}
	\begin{equation}
		S_{Amdahl} = \frac{T_{s}}{T_{p}} = \frac{1}{\alpha + \frac{(1 - \alpha)}{p}} \qquad
		S_{Gustafson} = \alpha + (1- \alpha) p
	\end{equation}
\end{framed}
\end{comment}
The execution time decreases by a factor \textit{close} to the number of parallel instances: still, this value is not the same because the parallelization degree does not influence the time to boot up the framework. Moreover, we force the utilization of only one worker\footnote{This part could be considered by all means as a sequential section} during the merging of all the intermediate priority queues into a global $k$ nearest neighbours list.\\
The sequential program performs slightly better than the Flink counterpart with parallelism set to one because there is some overhead due to the creation of the management system: both \emph{Flink} and \emph{Spark} have their own pseudo-Yarn environment\footnote{It is quite similar to \textsf{YARN} and the programmer can even make the frameworks switch to the latter so that they can run seamlessly on top of an up-and-running Hadoop cluster} to deal with the workers.\\
The performance of our \textsf{CollaborativeCluster} depends on the slowest machines in the cluster but we have set an upper bound by fixing a timeout such that \textsf{MajordomoNode} stops waiting after five minutes from its start.\\
The thyroid dataset is small thus even when we take only one half of it (e.g. Single Node \textsf{First Split} or \textsf{Second Split}) we will not see a proportional decrease in the execution time.\\
This occurs because we are really close to the minimum time to fire up the framework and because we need to calculate the value ranges for each feature by surveying all the training set instances, since we usedGower distance (eq. \ref{eq:gower} in Chapter \ref{CH_ML}).\\
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{ccccc}
			\toprule
			 Dataset & Size of cluster [, \textsf{Split}] & average time & minimum time & maximum time \\
			\midrule
			Whole dataset & Single Node & 10.87 s & 10.65 s & 20.23 s\\
			\midrule
			\multirow{3}{*}{Balanced Biased} & Two Nodes & 14.96 s & 14.24 s& 16.42 s\\
			{} & Single Node, \textsf{First Split} & 10.90 s & 10.66 s & 11.79 s\\
			{} & Single Node, \textsf{Second Split} & 10.86 s & 10.66 s & 11.57 s\\
			\midrule
			\multirow{3}{*}{Unbalanced Biased} & Two Nodes & 14.93 s & 14.16 s& 18.56 s\\
			{} & Single Node, \textsf{Split 60} & 10.81 s & 10.65 s & 11.34 s\\
			{} & Single Node, \textsf{Split 40} & 10.87 s & 10.70 s & 11.29 s\\
			\bottomrule
		\end{tabular}		
	\end{adjustbox}
	\captionsetup{justification=centering}
	\caption[Thyroid performance evaluation]{Thyroid KNN running time for \emph{Apache Flink} \\ with $k=1$ and using Gower distance as metric}\label{tab:flink_thy}
\end{table}
\\The need for these value ranges makes a \textsf{KNN} employing Gower distance slower than one using the Euclidean one: this is proved in fig \ref{fig:susy_slackware_runtime} where we see that working with the latter distance a dataset composed by $\approx250K$ instances takes less than with $\approx7K$.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.70\textwidth]{images/Performance_slack_noZero}
	\captionsetup{justification=centering}
	\caption{SUSY KNN Performance on the faster machine (A)}\label{fig:susy_slackware_runtime}
\end{figure}
\\ \noindent The tests were executed on two machines $A$ and $B$ (tab. \ref{tab:techSpecs}): $B$ was slower mainly because the experiment took place on a live distro.\\
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=0.85\textwidth}
	\begin{tabular}{rcc}
	\toprule
	{} & Faster Machine (A) & Slower Machine (B) \\
	\midrule
	OS: & Slackware 14.1 & Slackware-Current Live \\
	CPU: & Intel Core i7 @2.40 GHz (quadcore) & Intel Core i7 @2.80 GHz (dualcore) \\
	CACHE: & 6MB & 4 MB \\
	RAM: & 16 GB & 8GB \\ 
	DISK: & 1TB 5400rpm & 750GB 7200rpm\\
	\bottomrule
	\end{tabular}
	\end{adjustbox}
		\captionsetup{justification=centering}
				\vspace{-2pt}
		\caption{Technical specifications of the machines used for testing \textsf{CollaborativeCluster}}\label{tab:techSpecs}
\end{table}
\noindent\\The KNN algorithm occupies a preponderant share of the whole execution time but we have to consider that inside the average time there are also:
\begin{enumerate}
	\itemsep0em 
	\item the initialization of the \textsf{RequestActor} and of its \textsf{ActorSystem} (\S \ref{client_side}) to send the query (~0.3 s\footnote{Obtained by executing \textsf{time} on the \textsf{client} executable without giving all the necessary parameters thus forcing its immediate shutdown});
	\item the message passing and the instantiation of a global\textsf{MajordomoActor} and of a \textsf{BackendNode} for each cluster node (\S \ref{cluster_node});
	\item the transferring via Akka TCP Streams of the intermediate results;
	\item the execution of a small program to perform the last step of the classification on the intermediate results by majority vote (\S \ref{collecting_results_scala}) (~0.3 s to 1.5 s with \textbf{cold cache}\footnote{If the cache is empty (no reference to the intermediate results file and the Scala \textsf{collect} program) we will have an higher value than with a \textbf{warm cache}});
	\item the termination of the \textsf{RequestActor} and its \textsf{ActorSystem}.
\end{enumerate} 
Even if $B$ is slower, we can see from tab. \ref{tab:flink_SUSY} that the splitting of the dataset makes the distributed execution time shorter than the centralized one with the whole dataset.\\
A comparison between the average execution times of two machines can be seen in fig. \ref{fig:slow_runtime}.
\begin{figure}[h!]
	\centering
	\vspace{-10pt}
	\includegraphics[width=0.9\textwidth]{images/Performance_slow_noZero}
		\vspace{-10pt}
	\caption{Performance on the slower machine (B)}\label{fig:slow_runtime}
\end{figure}

\newpage
\clearpage
\begin{table}[H]
	\centering
	\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{ccccc}
			\toprule
			Dataset & Size of cluster [, \textsf{Split}] & average time & minimum time & maximum time \\
			\midrule
			Whole dataset & Single Node & 34.84 s & 34.02 s &35.83 s\\
			\midrule
			\multirow{3}{*}{Balanced Biased} & Two Nodes & 24.40 s & 22.54 s & 26.98 s\\
			{} & Single Node, \textsf{First Split} & 13.10 s & 12.91 s & 13.45 s\\
			{} & Single Node, \textsf{Second Split} & 13.49 s & 13.21 s & 22.19 s\\
			\midrule
			\multirow{3}{*}{Balanced Unbiased} & Two Nodes & 24.51 s & 22.77 s & 25.93 s\\
			{} & Single Node, \textsf{First Split} & 15.29 s & 14.62 s & 22.91 s\\
			{} & Single Node, \textsf{Second Split} & 15.42 s & 14.64 s & 23.13 s\\
			\midrule
			\multirow{3}{*}{Unbalanced Biased} & Two Nodes & 31.22 s & 29.42 s & 34.64 s\\
			{} & Single Node, \textsf{Split 60} & 15.73 s & 15.23 s & 26.24 s\\
			{} & Single Node, \textsf{Split 40} & 14.36 s & 13.88 s & 24.83 s\\
			\midrule
			\rowcolor{lightgray}
			\multirow{4}{*}{Unbalanced Unbiased} & Single Node, \textsf{Whole Dataset} & 17.92 s &	17.20 s & 25.92 s\\
			{} & Two Nodes & 22.41 s & 21.09 s & 34.99 s\\
			{} & Single Node, \textsf{Split 60} & 14.39 s & 13.79 s & 20.98 s\\
			{} & Single Node, \textsf{Split 40} & 15.65 s & 15.01 s & 22.80 s\\
			\bottomrule
		\end{tabular}		
	\end{adjustbox}
	\captionsetup{justification=centering}
	\caption[SUSY performance evaluation]{SUSY KNN running time for \emph{Apache Flink}\\ with $k=11$ and using Euclidean Distance as metric}\label{tab:flink_SUSY}
\end{table}

%\clearpage
%\newpage

\section{Possible improvements} 
\subsection{Thyroid}\label{improving_thyroid}
The main issue with KNN while using the Thyroid dataset is that we have a substantial imbalance between the number of instances belonging to class $3$ (constituting more than the 90\% of the training set) and the examples related to the other two classes. \\
Thus, to enhance the accuracy we need to focus on \emph{Rare Case Learning} techniques such as \textbf{\textit{example reweighting}} and \textit{\textbf{example resampling}}.\\
In both cases we are attaching a misclassification cost to the classes so instead of maximizing the usual accuracy we will focus on the \textit{cost-weighted} accuracy: if we classify wrongly a rare class we will have a larger cost than if it was a normal class.\\
We could employ \textit{example reweighting} when we extract the classification label from all the intermediate data of the different cluster nodes: in this case instead of following the majority vote algorithm in \S \ref{collecting_results_scala}, our \textsf{Gatherer\-Actor}\footnote{It is the actor in charge of this operation, as explained in \S \ref{majordomoactor}} could promote rare classes by dividing (distances) or multiplying (similarities) by a factor their related instances.\\
\textit{Example resampling} involves major modifications to our dataset as we should either \textit{downsample} the most frequent class or \textit{oversample} the rarer ones. \\
The first technique cuts down the examples of the recurrent class but does not improve what the classifier infer from the uncommon classes' instances.\\
Otherwise, we could turn to \textit{oversampling} by adding new synthetic instances to the less frequent classes\footnote{Repeated samples are not advisable as they usually bring about overfitting}. For example, the SMOTE approach looks at the minority example's $k$ nearest neighbours of the same class and creates new instances by sampling points laying on the lines that connect the example to its neighbours: we would increase the accuracy of our KNN classifier by exploiting the outcome from another KNN.\\
There is not much we can do about the execution time as we are already too close to the minimum amount of time necessary for the whole system to fire itself up.

\subsection{SUSY}
SUSY accuracy could only be raised by applying advanced Machine Learning techniques that can make the most of the visible and hidden interconnections between the features. As a matter of fact, the dataset was released after the publishing of a paper \cite{SUSY_dataset} exploiting Deep Learning, in particular deep neural networks, to enhance the accuracy achieved with boosted decision trees and shallow neural networks.\\
We should rethink which information would be exchanged in the cluster in order to keep the dataset secret: we could for example exchange just the label and the per-class accuracy such that it could be use as a weighting factor.\\
To speed up the execution time we need to add preprocessing steps: e.g. we could focus on creating efficient structures as \textsf{kd-trees} but we should find a way to store them once the computation is finished so that we could reload them the following time.\\
Another possibility would be to store and retrieve the model created with the aforesaid machine learning techniques during each query: since we would not need to scan the whole training set, the classification task should run faster.

\subsection{\textsf{CollaborativeCluster} architecture}
So far the architecture is \textbf{fail-safe} so we could improve it by modifying it in order to follow a \textbf{fail-recovery} scheme: we would enable the persistence of our actors and to do that we should assign identifiers that do not change across different incarnations (or we would have the same problem of \textsf{ActorRef} as explained in \S \ref{akka_actors}).\\
Persistence would complicate the design of the \textsf{CollaborativeCluster} that essentially follows a kind of \textit{fire-and-forget} strategy without Scala \textsf{Futures} or \textsf{Ask} patterns but it would increase its reliability.

\section{Future Work}
Apart from the possible improvements exposed in the previous section, there is another possible line of research that would deserve to be explored: moving from a batch-processing approach to a streaming or, even better, a hybrid one.\\
The architecture of the \textsf{CollaborativeCluster} would have to be re-shaped because instead of triggering the execution of the Machine Learning task every time that a query is received, the streaming processing program would need to be always up-and-running. 
Let us illustrate how such a cluster could be employed for example in the Healthcare sector: after patients were triaged by EMS\footnote{Emergency Medical Services} paramedics, they could be assigned according to their symptoms to hospitals whose emergency rooms and specialized departments could be reached quickly and that are not crammed.\\
Each time someone is admitted in an emergency room we update the status of the hospital and try to foresee also how it will affect the hospital beds' count in the departments. A setting like this would be extremely helpful in case of large-scale disasters because it will help to provide the best usage for the structures (it would be a load balancing approach) while guaranteeing no life-threatening delays for the patients: an admission based on their clinical needs would ensure that after they passed through the ER\footnote{Emergency Room} they could continue their hospitalization in the same place in the required specific department.\\
To set up a hybrid cluster we would need:
\begin{itemize}
	\item a framework like \emph{Apache Kafka} to handle the re-playable stream of incoming/outgoing patients;
	\item some platform for batch processing to help determine which treatment should be followed and to which department the patient should be admitted;
	\item a connector to bind together the cluster with the already-up-and-running stream processing job to cut down query response time in order to achieve finally a real-time asynchronous system.
\end{itemize}

\section{Conclusions}
Implementing collaboration pays off especially when we have to cope with \textit{rare class learning} (our Thyroid dataset): by putting together the insights from two datasets we achieve the same accuracy level that we would have with the whole dataset.\\
The difference in accuracy between a single node and a clustered execution is narrow when we do not have relevant data skewness (our SUSY dataset) and all the training sets of the cluster nodes are more or less balanced: there is a discernible improvement only if the dataset distribution in two splits is \textbf{unbalanced} and \textbf{biased}.\\
It is worth noticing that, no matter which data distribution we have, a clustered execution with two partial sets is faster than one with a single node holding the whole dataset while bearing the same result in terms of accuracy.\\
One of the strengths of having developed \textsf{CollaborativeCluster} in Akka is that it runs directly on the Java Virtual Machine so we really can pledge to the motto \textquotedblleft \textit{write once, run everywhere}\textquotedblright. In addition to this, the conciseness of Scala and its built-in pattern matching features cuts down on the verbosity of the \textsf{Actors}, that we would have had by adopting Java, in favour of code that is succinct and clear to understand.\\
Looking backwards, the adoption of Scala as a unifying language was wise because it is a natural match also for Big Data frameworks: \textit{placeholders}, \textit{closures}, serializable \textit{tuples} and all the available operations for collections (e.g. \textsf{map}, \textsf{flatMap}, \textsf{reduce}) allow programmers to write condense yet straightforward code.\\
When talking of \emph{Apache Flink} and \emph{Apache Spark}, it is not accurate to say that they \textit{will revolutionize} the Big Data field simply because they \textit{have already done that}: their richness of features can satisfy the requests of enterprises that cannot afford infrastructures suited for deep learning techniques or whose size of datasets is not as colossal as the one generated by social networks.\\
It is not an easy choice to elect between these two rapidly evolving options because \emph{Apache Spark} has a mature ecosystem with a deluge of libraries and connectors whereas \emph{Apache Flink} stream processing and performance looks very promising.\\
One of the problems that prevented us from using more advanced Machine Learning techniques was finding a way to unambiguously sent the model computed by a node to another one: this issue is near to be solved as, for example, \emph{Apache Spark 2.0} \cite{Spark2} will offer also a way to store and reload a Machine Learning model by employing an \textit{exchangeable storage format} that could be used with whichever language we want. From this serialization that is restrained to a particular framework we could work on how make the format general enough to be understood by or translated for other tools.\\
Although we yearn for interoperability, defining widely-accepted standards is the elephant in the room: new frameworks and services steadily come up to solve problems that we did not know we had so far, thus the real challenge does not reside in finding a solution for a peculiar request but in how to make it work with the other pieces of the system already in place\footnote{As it is happening in the US healthcare industry, see \S \ref{health_sharing}}.