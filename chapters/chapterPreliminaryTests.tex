\begin{savequote}[75mm]
	\textquotedblleft Hoping for the best,\\ prepared for the worst,\\ and unsurprised by anything in between.\textquotedblright
	\qauthor{Maya Angelou, \textit{I Know Why the Caged Bird Sings}}
\end{savequote}
\chapter[Datasets' preliminary evaluation]{A preliminary evaluation of our datasets}

\section{A survey of the thesis' datasets}\label{survey_ML_datasets}
After the brief introduction to Machine Learning in Chapter \ref{CH_ML} we can look into the two datasets that have been employed for this thesis. The previously explained concepts will prove to be useful during this concise overlook of the KNN classifier performance over medium-sized test sets: this represents our blueprint to understand the results of the \textsf{CollaborativeCluster} that will be studied in depth in Chapter \ref{conclusion_chapter}.\\
The code for the following tests was examined in \S \ref{accuracy_whole} and it was run in the \textit{Spark Standalone Mode}\footnote{This deployment mode consist in creating a master node and multiple workers on the same machine. Everyone has its own IP address and we can specify settings for each worker in the \textsf{conf} folder by modifying \textsf{spark-env.sh}} on a server whose specifics can be found in tab \ref{tab:techSpecsServer}.
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{rc}
			\toprule
			OS: & Ubuntu Server (Fluxbox as WM) \\
			CPU: & Intel Xeon CPU E5-2670 @ 2.60GHz (16-core)\\
			CACHE: & 20MB \\
			RAM: & 100GB\\ 
			DISK: & 4TB 7200rpm\\
			\bottomrule
		\end{tabular}
	\end{adjustbox}
	\captionsetup{justification=centering}
	\caption{Technical specifications of the server used for the preliminary tests}\label{tab:techSpecsServer}
\end{table}
\\The test were carried out by creating eight instances of workers (note that the number of workers does not mean job parallelism): however Spark was allowed to take as many cores as possible during the execution according to the server workload.  
\subsection{Thyroid}
The whole dataset is composed by 7200 instances and the number of possible classes is three. Each instance has 15 binary and 6 continuous attributes, for a total of 21 features.\\
There is no suggestion related to the importance of each feature so we will consider all the dimensions when calculating the distance from a test example.\\
The dataset has been split into training and test sets using different percentages: \textsf{90-10}, \textsf{80-20} and \textsf{70-30} percent.\\ 
Since we have mixed features we expect to have better performance when using Gower's distance than the Euclidean one and that the bigger the training set is, the more accurate will be our classifier: let us look at the results in fig. \ref{fig:allthyroid}.
\begin{sidewaysfigure}
	\includegraphics[width=\textwidth]{images/thyroid_ALL}
	\caption[Thyroid dataset evaluation with different size and $k$]{Thyroid Dataset: accuracy level with respect to increasing $k$ and training set size}\label{fig:allthyroid}
\end{sidewaysfigure}
\\The level of accuracy seems pretty high but we need to focus on the dataset distribution that is quite unbalanced: for example, in the training set derived by a 90-10 split $92\%$ of the instances belong to class 3, $5\%$ to class 2 and only $2\%$ to class 1.\\
Also the related test set is unbalanced: $93\%$ of the instances belong to class 3, $4\%$ to class 2 and $3\%$ to class 1 (see tab. \ref{tab:thyroid_composition}).
\begin{table}[h!]
	\begin{center}
		\begin{adjustbox}{max width=\textwidth}
			\begin{tabular}{lcccccc}
				\toprule
				Training set & \specialcell{Class 1 \\instances} & \specialcell{Part-to-Whole \\ ratio} & \specialcell{Class 2 \\instances} & \specialcell{Part-to-Whole \\ ratio} & \specialcell{Class 3 \\instances} & \specialcell{Part-to-Whole \\ ratio} \\
				\midrule
				\textsf{Split 90} & 148 & 2\% & 341 & 5\% & 5991 & 93\% \\
				\textsf{Split 80} &  132 & 2\% & 302 & 5\% & 5326 & 93\% \\
				\textsf{Split 70} & 120 & 2\% & 250 & 5\% & 4670 &93\% \\
				\textsf{Test set} & 18 & 3\% & 	27 & 4\% & 675 & 93\%\\
				\bottomrule
			\end{tabular}
		\end{adjustbox}
	\end{center}
	\caption{Composition of the Thyroid dataset}\label{tab:thyroid_composition}
\end{table}
\\Given this unbalance, the \textbf{per-class accuracy} (see eq. \ref{eq:peraccuracy}) would paint a very bleak picture if we simply were to divide by the number of classes because of the -understandable- low accuracy level when we have to classify instances of class 1 and 2.\\
Therefore the best evaluation method would be still the simple \textbf{accuracy} in the eq. \ref{eq:weightedperaccuracy} because it takes into account (indirectly) also the cardinality of the classes.\\
It is not surprising that with this imbalance and the bias we only achieve good accuracy if and only if we select a small value for $k$: since setting $k=1$ scored the best performances throughout different experiments, the tests run on the \textsf{CollaborativeCluster} will use this value and \textbf{Gower's distance} as metric.
\begin{comment}
\begin{sidewaysfigure}
\includegraphics[width=\textwidth]{../../Dropbox/MSc_Thesis/images/thyroid_composition}
\caption{Composition of Thyroid's datasets}	\label{fig:allthyroid_composition}
\end{sidewaysfigure}
\end{comment}

\subsection{SUSY}\label{susy_whole_ml}
The SUSY\footnote{SUSY stands for SUperSYmmetric} dataset has 5000000 items: each instance represent a collision and its associated readings and can be classified either as $\mathbf{0}$ (\textsf{background} noise) or as $\mathbf{1}$ (\textsf{signal}) if it produced electrically-charged supersymmetric particles.\\
We must handle 18 \textbf{numerical} features whose last ten are derived from the first eight to help with the classification task: for this reason we have tested the performance of our classifier in terms of accuracy both with feature selection and without.\\
Exploiting feature selection should lead us to an evident improvement but unfortunately both with our KNN and with the deep learning techniques used in \cite{SUSY_dataset} the increase was clear but not substantial (e.g. 1-3\% in our KNN see Table \ref{tab:susy_featsel} ): of course, we notice that the overall accuracy with deep learning techniques is better than a simple KNN (up to a 10\% gap, see Table 2 in \cite{SUSY_dataset}) but that was quite obvious and advanced machine learning techniques are out of the scope of the thesis.
\begin{table}[h]
	\begin{center}
		\begin{tabular}{lcc}
			\toprule
			Training set & \specialcell{best achieved accuracy\\ \textbf{without feature selection}} & \specialcell{accuracy \textbf{with feature selection} \\given the same $k$ and metric}\\
			\midrule
			\textsf{Split 90} &  0.77 & 0.779 \\
			\textsf{Split 80} &  0.76 & 0.79 \\
			\textsf{Split 70} &  0.76 & 0.79 \\
			\bottomrule
		\end{tabular}
		\caption{\textsl{KNN} results without and with feature selection over SUSY dataset}\label{tab:susy_featsel}
	\end{center}
\end{table}
\\While reading the results of tab. \ref{tab:susy_featsel} (where \textsf{Split 90}, \textsf{Split 80} and \textsf{Split 70} means that we are testing with the 90\%, 80\% and 70\% of all the training set instances) we should keep in mind that:
\begin{itemize}
	\item  we draw our best $k$ value from the interval of $[1,13]$ and by no means the achieved $k$ is proved to be the overall \textbf{optimum} value;
	\item we are comparing the results of the same training set without and with feature selection given the metric and the value of $k$ that offered the best accuracy in the former circumstance, therefore these values may easily not be optimal parameters in the case feature selection.
\end{itemize}
We can see in tab. \ref{tab:susy_size} that the Euclidean distance is a more suitable metric for this dataset than Gower distance because we do not have categorical features and that the accuracy increases with the size of the training set.
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{lcccr}
			\toprule
			Training set & no. instances & best achieved accuracy & value for $k$ & metric\\
			\midrule
			\textsf{Split 90} & 4500000 & 0.77 & 11 & Euclidean \\
			\textsf{Split 80} & 4000000 & 0.76 & 13 & Euclidean \\
			\textsf{Split 70} & 3500000 & 0.76 & 13 & Euclidean \\
			\bottomrule
		\end{tabular}
		\caption{$KNN$ Results with respect to SUSY dataset}\label{tab:susy_size}
	\end{center}
\end{table}
\\Another thing that was to be expected is that, because the SUSY dataset has roughly the same number of instances between the two classes, we can achieve better accuracy just by increasing $k$.
\\Therefore we will use for our experiments in the \textsf{CollaborativeCluster} euclidean distance as a metric and $k\ = \ 11$ because we will use the instances of Split 90 to observe what happens when we have to deal with all the possible biased-unbiased, balanced-unbalanced dataset compositions.\\
Although we have no remarkable bias in terms of the dataset internal composition, we should look more deeply into our results' details: if we mind just the overall accuracy we would miss the difference in the achieved accuracy for each class.\\
We can narrow the analysis down to the outcomes of the classification task given the aforesaid parameters for the \textsf{CollaborativeCluster}: the collected results are presented in tab. \ref{tab:susy_accuracy_detail}.
\begin{table}[h]
	\centering
	\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{ccc|ccc|ccc}
			\toprule
			\multicolumn{3}{c}{Total} & \multicolumn{3}{c}{Class $0$} & \multicolumn{3}{c}{Class $1$} \\
			\midrule
			accuracy & \specialcell{correct} & \specialcell{no. instances} & accuracy & \specialcell{correct} & \specialcell{no. instances} & accuracy & \specialcell{correct} & \specialcell{no. instances}\\
			\midrule
			0.77 & 770 & 1000 & 0.90 &464 &514& 0.63 &306 &486 \\
			\bottomrule
		\end{tabular}
	\end{adjustbox}
	\caption{A close look at SUSY's accuracy results}\label{tab:susy_accuracy_detail}
	\vspace{-10pt}
\end{table}
\\Our KNN classifier recognizes with more accuracy a collision whose outcome should be classified with $\mathbf{0}$ (\textbf{\textit{background}}, fig.\ref{fig:SUSY_particles}(\textbf{b})), than one that should be labelled with $\mathbf{1}$ because it shows signs of the production of electrically-charged supersymmetric particles (\textbf{\textit{signal}}, fig.\ref{fig:SUSY_particles}(\textbf{a})). Why?\\
To determine the outcome of every collision (no matter if is background or signal) we must take into account not only what can be observed (there are always two charged leptons $\ell^{+}$, $\ell^{-}$ see fig. \ref{fig:SUSY_particles}), but also \textit{what is missing} (energy amounts) from which we must infer the presence of what \textit{is not visible} ( neutrinos $\nu$, electrically-charged $\chi^{\pm}$ or electrically-neutral $\chi^0$ supersymmetric particles).
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.50\textwidth]{images/SUSY_Outcomes}
	\caption{Collision diagrams for SUSY benchmark taken from \cite{SUSY_dataset}}\label{fig:SUSY_particles}
\end{figure}
\\The first six low-level input features are determining the kinematic features of the leptons\footnote{For each lepton we have its $\eta$ and $\phi$ (we could think of them as axis of a space) as well as its transverse momentum $pT$. If we make two particles collide and the vector sum of all the transverse momentum $pT$ of the resulting particles is not zero than we could infer that there might be an invisible particle involved in it.} and then we have two values that, in addition to the transverse momentum $pT$ of the leptons, can help us establishing whether we may account invisible particles and which ones (in the fig. \ref{fig:SUSY_particles}) for the missing energy.\\
A KNN does not pay attention to physics' rule so it is missing many latent dependencies that could help the classification: if we look at the ranges of the features' possible values, we see immediately that above some certain thresholds our test example can not be mistaken for a background instance.\\
\begin{table}[h]
	\centering
	\begin{adjustbox}{max width=0.7\textwidth}
		\begin{tabular}{rcl}
			\toprule
			\multicolumn{3}{c}{Ranges of values with respect to the $7^{th}$ feature}\\
			\midrule
			\specialcell{Instances' Class} & Min Value & Max Value\\
			\midrule
			Class 0 \& 1 &  0.0002598711 & 21.0688762665 \\
			Class 0 & 0.0007088768 & 11.3011627197 \\
			Class 1 & 0.0002598711 & 21.0688762665 \\
			\bottomrule
		\end{tabular}
	\end{adjustbox}
	\caption{SUSY's $7^{th}$ feature range values}\label{tab:ranges_susy}
\end{table}

If we had a test instance whose seventh feature value was equal to $11.40$, the KNN would leave the decision to the most frequent label among our nearest neighbours even if we already know for sure that the test could not be of class 1(see fig. \ref{fig:susy_range_box}).
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.80\textwidth]{images/seventh_figure}
	\vspace{-10pt}
	\caption[SUSY's range of values for \textit{missing energy magnitude}]{Boxplot with the observed values for \textit{missing energy magnitude} ($7^{th}$ feature)}\label{fig:susy_range_box}
\end{figure}
