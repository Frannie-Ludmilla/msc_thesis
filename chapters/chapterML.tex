\begin{savequote}[75mm]
\textquotedblleft The unseen harmony is better than the visible.\textquotedblright
\qauthor{Heraclitus, \emph{Fragment no 54}}
\end{savequote}
\chapter{Machine Learning}\label{CH_ML}
\section{A few words on Machine Learning}
Let us start from philosophy and the concept of \textbf{inductive reasoning}: we make specific observations and we analyse them in order to find an hidden law that explains why something works or happens in a peculiar way.\\
Staring at all the individual instances at a time may baffle the observer because too much information could be present: therefore we need to abstract ourselves from the whole and to look for regularities that help us to detect latent patterns.
Once we think we have found a pattern, we formulate hypotheses, we check them and we try to draw conclusions either by creating a model or a generalized theory that can help us to make predictions about the future.\\
Essentially we start with an analysis \footnote{\label{DIC}Definition taken from the on-line Oxford Dictionary: \textit{"a detailed examination of the elements or structure of something"}} and we end up with a synthesis \footnote{ibidem: \textit{"the combination of components or elements to form a connected whole"}}.\\
Given that the first statement of these ideas dates back to Aristotle, we cannot say that Data Analysis is \textit{something new}, yet what has changed lately is that we want our reasoning efforts to be performed in an automated way by anything that could crunch data at an higher speed than our brain.\\
And here, we come back to computing and to Machine Learning.\\
Even if there are three types of Machine Learning (\textbf{supervised}, \textbf{unsupervised} and \textbf{reinforcement learning}) we will focus only on the first category (also called \textbf{predictive}) because what we want to do with our datasets is to solve a \emph{Classification} problem: given an instance of an object we must predict to which class it belongs.\\
If there are only two possible classes our problem is accredited as \textbf{binary classification}, otherwise we must differentiate between \textbf{multi-class} and \textbf{multi-label} definitions \cite{Bishop}  (see box below).
\begin{framed}
	\noindent When the cardinality of the set $C_n$ of possible labels/classes is higher than two, we refer to such classification tasks as
	\begin{description}
		\item[\textit{multi-class:}] if an instance belongs to \textbf{only one} class $\in C_n$ with $n>2$ (e.g. a cat is an instance of the \textit{Felidae} family but it cannot belong also to the \textit{Canidae} one)
		\item[\textit{multi-label:}] if \textbf{multiple labels} can be assigned \textbf{simultaneously} to an instance (e.g. a person could be labelled as \textit{student} and at the same time as a \textit{worker} or even as a pensioner because these classes are not mutually exclusive)
	\end{description}
\end{framed}
\noindent Each \emph{instance} in ML is composed by a \textbf{label} (the classification value) and a \textbf{feature vector} that embodies the characteristics of the object/instance: these features can be numerical (integers or real numbers), binary (e.g. a boolean \textit{true} or \textit{false}) or categorical (when they belong to a discrete set of values, e.g. $\{blue, green, red\}$ $\in Colours$).\\
We use already classified instances as a \textbf{training set} to be fed and digested into our ML algorithm such that, given an object, we could predict either one of its numerical features (we refer to this kind of problem as \textbf{Regression}) or its class.\\
We could formalize our problem as a \textbf{function approximation}: we want to find a function $f(x)=y$ such that for any vector $x$ of features belonging to an object, it predicts the most likely value of the object's label, $y$.
The function $f$ is unknown to us, so we must estimate it by observing our training set and build it in such a way that we have the minimum prediction error as possible: this will allow us to correctly classify inputs that we have not seen before.\\
Here comes in handy what we have read about inductive reasoning, because the more general the hypothesis is, the more this will be likely to \textit{transcend} the individual instances and to hold true also with regard to new observations.\\
This \textit{transcendent} behaviour from the training examples is necessary because we want to avoid a problem called \textbf{overfitting}: let us define it as when a model becomes too strictly tailored for the training set that it performs poorly with new instances or with a test set.\\
It is true that the features of an object/instance represent its characteristics, still some features are more relevant than others for our classification purpose, therefore we could pick out only the ones that are meaningful to us (\textbf{feature selection}).
For example, if we want to classify a book into a genre, we can look at books with the same or a similar author, publisher and publishing year: defining an affinity between books due to their amount of pages is not likely to help us.\\
This feature selection step is quite crucial because there are some features -as the number of pages in the former example- that could lead us astray during our classification and we refer to them as \textbf{noise features}.\\
Now we can sketch an easy example where \emph{overfitting} brings us to a wrong classification: let us imagine that we have a training dataset where all the books published in the latest two centuries and written in English whose length is greater than a thousand pages belongs mainly to the fantasy genre. If we were to classify therefore the Ulysses by Joyce, our noise feature will probably move us to the quite questionable assertion that our test instance is once again a lengthy fantasy book.\\
\subsection{A walk-through along distance and similarity functions}
Another question arise: how do we define an \textit{objective} approach to asseverate the affinity between two objects?\\
Two instances are considered alike if it is either \textit{maximized the similarity} or \textit{minimized the distance} between their features.\\
Since we have features that belong to different domains, the distance or similarity function must be chosen carefully or our machine learning algorithm could have poor quality results.\\
With spatial coordinates we would employ the usual \emph{Euclidean} distance in \ref{eq:euclidean} (where $M$ is the length of $x$):
\begin{equation}\label{eq:euclidean}
	|\vec{x} - \vec{y}| = \sqrt{\sum_{i=1}^{M}(x_i -y_i)^2}
\end{equation}
Both the Euclidean and the Manhattan distance are special cases (for $p=2$ and $p=1$ respectively) of the more general $\mathbf{L_p}$-norm in \ref{eq:lnorm}.
\begin{equation}\label{eq:lnorm}
	|\vec{x} - \vec{y}| = \left( \sum_{i=1}^{M}(x_i -y_i)^p \right)^\frac{1}{p}
\end{equation}
Unfortunately using these formulas has a drawback when one of the attributes has a relatively large range because it can overpower the other attributes and will have more influence during the computation.
Therefore we could \textit{normalize} the two vectors $x$ and $y$ by dividing them by their euclidean length $L (\vec{v})= \sqrt{\sum_{i=1}^{M}(v_i)^2}$; essentially this will bring us to outcomes with values' ranges close to those of the cosine similarity between the two vectors.
\begin{equation}
	cosine\_sim(x,y)= \frac{\sum_{i=1}^{M} x_i \cdot y_i}{\sqrt{\sum_{i=1}^{M} (x_i)^2 \sum_{i=1}^{M}(y_i)^2}}
\end{equation}
However, it may happen that these formulas bring results that do not have sense at all: for example if we used Euclidean distance with categorical or binary features.\\
%Let us clarify with an example: if we have features as $gender$ with values $0$ as female, $1$ as male  two instances will be similar if and only if  
%we want to set zero distance if and only if this feature in the two instances.
% we do not want to have an \textit{in-between} distance but we want to have zero distance only when two instances are equal and maximum in all the other occurrences.\\
In addition to this, it happens frequently that our instances have \textbf{mixed} features (partly binary and partly numerical as in our Thyroid database) so sometimes we would like to choose a distance/similarity formula that smoothly takes into account both types of features and even let us assign different weights to the features as Gower distance does \cite{Gower71}(see equation \ref{eq:gower}).
\begin{equation}\label{eq:gower}
	GowerDist_{i,j} = \frac{\sum_{k} \left(w_{ijk} \cdot D_{ijk}\right)}{\sum_{k} w_{ijk}}
\qquad	D_{ijk}= \frac{|x_{ik} - x_{jk}|}{R_k}
\end{equation}
The actual distance is the numerator of $D_ijk$ and it is normalized by the $R_k$, the range of the possible values of that attribute $(R_K= x_{max} - x_{min})$; each feature has also a weight $w_{ijk}$ such that we can define whether it influences our calculated distance or not (we need just to set for that $k$ its related $w_{ijk}$ to $0$).\\
Is there an always best choice in terms of distance/similarity formulas? \\
Short answer: No.\\
Long answer (a.k.a. Machine Learning-aware re-adaptation of the \textbf{No free lunch theorem}): what we usually do is to \textit{empirically} find out which is the best tool for classification on our particular dataset but there is no \textit{passe-partout} algorithm or formula always guaranteeing the best performance in every possible domain.

\section{K Nearest Neighbour}
The simplest algorithm that comes to mind when we talk about classification is the K Nearest Neighbour that retrieves the most likely classification label by looking at the labels of the \textit{closest} $k$ neighbours and by selecting the one that has the most occurrences. If our $k$ is set to one, we simply select the class label of the most similar instance in the training set.\\
The algorithm does not try to sketch out clearly the target function $f(x)$ and for each classification task of an instance $x_i$ we simply re-calculate $x_i$'s closest neighbours from scratch by scanning once again the training set. For this reason the KNN Classifier is considered an \textbf{instance-based} (the training set must be stored and surveyed in its entirety) and \textbf{lazy} (computation is delayed until a new classification task comes forward) learner.\\
This evergreen and quite straightforward technique (see pseudocode in alg. \ref{alg:pseudo_knn}) gives us an effective classifier that is, however, still in need of refinement: as a matter of fact, we have to find which is the optimum $k$ that provides the best accuracy for our dataset.\\
Moreover, since our computation time is bounded linearly to the training set's size, we should ask ourselves whether it makes sense or not to perform some preprocessing step to create data structures (e.g. \textsf{kd-trees}) that could unburden the computational cost of the cycle (lines from 4 to 6 in Algorithm \ref{alg:pseudo_knn}).
\begin{algorithm}
	\caption{Pseudo-code for K-Nearest Neighbour}\label{alg:pseudo_knn}
	\begin{algorithmic}[1]
		\Procedure{KNN}{\textit{k} , T: {\small training set} , \textit{x}: {\small instance to classify}}
		\State \textit{size} $\gets$ length of T 
		\State double \emph{DistancesWithLabels}[\textit{size}] = 0
		\ForAll{$t_i \in$ T}
		\State $d_{i} \gets$ Compute distance $d(t_{i},x)$
		\State add $<d_{i}, f(t_i)>$ to \emph{DistancesWithLabels}
		\EndFor
		\State \emph{TopK} $\gets$ \textit{k} smallest components w.r.t. $d$ in \emph{DistancesWithLabels}
		\State \Return \textit{label} $\gets$ majority vote over \emph{TopK}
		\EndProcedure
	\end{algorithmic}
\end{algorithm}
\\We said that if you \textbf{train too much} your classifier (or \textit{too little}) you are doomed to have \textbf{overfitting} (or \textit{underfitting}), now it is time to meet another Machine Learning ordeal that also has an \textit{evil} sound: thy, wannabe data analyst, bow before the \textbf{curse of dimensionality}!
\subsection{Curse of Dimensionality}
The term \emph{dimensionality} refers to the size of our features' vector: an obvious statement is that the more attributes of an object/instance we store, the more information we have. It should be noted, nevertheless, that having more information \underline{\textit{does not equal}} to knowing better the object.\\
Inserting a feature as a characteristic of the instance corresponds to adding another dimension and the more dimensions we have, the more the idea of what distance means becomes fuzzier and fuzzier.\\
Let us think of a space composed by evenly-spaced points where we must randomly choose our $k$ nearest neighbours to break ties: if the space is one dimensional (like an arrow) we will have only two possible neighbours (the points at the left and at the right of it), if it was two-dimensional we would have four neighbours (left, right, top, bottom), in case of three-dimensions we would have six neighbours, as shown in fig. \ref{fig:neigh3d}, so if we had $d$ dimensions we would have exactly $2\cdot d$ same-distance neighbours between which we need to randomly pick out our $k$ nearest nodes.\\
Essentially, the more dimensions we have, the fuzzier it becomes the concept of distance.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.45\textwidth]{images/cube}
	\caption{Nearest neighbours in a 3d grid}\label{fig:neigh3d}
\end{figure}

\subsection{Improving KNN-classifier performance}
\subsubsection{Is accuracy a good evaluation metric?}\label{accur_measures}
In order to improve our classifier, we first need to establish a metric to see how well it performs. The simplest possible evaluation metric is given by looking at the ratio between the correctly-labelled instances and the number of the test instances (eq. \ref{eq:accuracy}), which we refer to as \textbf{accuracy}.
\begin{equation}\label{eq:accuracy}
	\alpha \ = \ accuracy= \frac{\# \ correct \ classifications}{\# \ total \ test \ instances}
\end{equation}
Still, accuracy may give us a distorted perception of the classifier's quality when classes are \textbf{imbalanced} (when in the training set we have more examples belonging to a class than instances of the other classes) so when we have $n$ classes we should take into account \textbf{per-class accuracy} (see eq \ref{eq:peraccuracy})\cite{accuracybook}
\begin{equation}\label{eq:peraccuracy}
per\textendash class \ accuracy = \ \frac{\Sigma \ accuracy\ for\ each\ class}{\# \ classes} \ = \frac{\sum_{i=1}^{n} \alpha(C_i)}{n}
\end{equation}
where $C_i$ refers to all the instances in our test set belonging to the $i^{th}$ class.\\
Unfortunately, \textbf{per-class accuracy} overlooks imbalanced classes in the test set: it is a simple average that does not take into account the fact that a class may have more instances in the test set than the other classes. Still, if we included also the cardinality of each $C_i$ in the formula in order to move to a weighted accuracy (in eq. \ref{eq:weightedperaccuracy}) we would come back to the simple accuracy (\ref{eq:accuracy}) that is, essentially, a weighted average.
% (we will prove it in \ref{eq:proof}, after).
\begin{equation}\label{eq:weightedperaccuracy}
weighted\ per\textendash class \ accuracy = \frac{\sum_{i=1}^{n} \alpha(C_i) \cdot |C_i|}{\sum_{i=1}^{n} |C_i|} \ = \ accuracy = \ \alpha
\end{equation}
\subsubsection{Other evaluation metrics for classifiers}
Let us assume that we devise a binary classifier acting as a spam filter that indiscriminately label all mails as spam: if we had to classify 1000 mails among which only 10 are not spam, our filter would still score 99.9\% of accuracy according to the equation in \ref{eq:accuracy}. Accuracy would paint a deceitful image whereas a  \textbf{per-class accuracy} would leave us with an ambiguous 0.5\% value that does not disclose anything about the actual robustness of the classifier.\\
We essentially want to find a metric that gives more importance to the correct classification of the non-spam mails that is the \textsf{True Positive} ($T_p$) case in tab. \ref{tab:conf_Matr_idea} where it is represented the confusion matrix related to our binary classification problem.\\
By looking at this matrix we can rewrite the accuracy formula as 
\begin{equation}\label{eq:accTP}
\alpha \ = \ accuracy= \frac{\# \ correct \ classifications}{\# \ total \ test \ instances} \ = \frac{T_p + T_n}{T_p + F_p + T_n + F_n}
\end{equation}
where the correct classifications value is made up by the sum of the correctly classified non-spam ($T_p$) and spam ($T_n$) mails.
\begin{table}[h!]
	\centering
	\begin{adjustbox}{max width=\textwidth}
		\begin{tabular}{l|r|c|c|c}
			\multicolumn{2}{c}{}&\multicolumn{2}{c}{\textsc{Actual}}&\\
			\cline{3-4}
			\multicolumn{2}{c|}{}&\textit{Normal}&\textit{Spam}&{}\\
			\cline{2-4}
			\multirow{3}{*}{\textsc{Predicted}}& \textit{Normal} & \specialcell{\textsf{True Positive} \\ ($T_p$)} & \specialcell{\textsf{False Positive} \\ ($F_p$)} & {}\\
			\cline{2-4}
			& \textit{Spam} & \specialcell{\textsf{False Negative} \\ ($F_n$)} & \specialcell{\textsf{True Negative} \\ ($T_n$)} & {}\\
			\cline{2-4}
			%\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$a+c$} & \multicolumn{1}{c}{$b+d$} & \multicolumn{1}{c}{$N$}\\
		\end{tabular}
	\end{adjustbox}
	\caption{Confusion Matrix}\label{tab:conf_Matr_idea}
\end{table}
\\ \noindent If we divide our $T_p$ by all the instances that were classified as \textit{Normal} we get the \textbf{Precision} ($P$) of our classifier; otherwise we could calculate its \textbf{Recall} ($R$) (also known as \textbf{Sensitivity}) to see among all the instances that are \textit{Normal} how many were correctly classified (see eq. \ref{eq:precReca}.)
\begin{equation}\label{eq:precReca}
	Precision = \ P = \ \frac{T_p}{T_p + F_p} \quad  , \quad Recall = \ R = \ \frac{T_p}{T_p + F_n}
\end{equation}
%The Recall metric represents essentially the accuracy with regard to a class whereas the Precision is the percentage among the correctly classified instances of a class.
In our example, we could infer that there is something fishy since we would have $0$ as the value for both recall and precision; however, for more balanced cases, if we take singularly these two metrics, they may fail to give us any insight of the actual robustness.\\
We can blend the aforesaid criteria into one metric by looking at the weighted harmonic mean of both \cite{Manning}: this brings us to the \textbf{F measure} whose formula is shown in eq. \ref{eq:fscore}. 
\begin{equation}\label{eq:fscore}
	F = \frac{1}{\alpha\frac{1}{P} \ + \ (1- \alpha)\frac{1}{R}} \ =  \ \frac{(\beta^2 +1) PR}{\beta^2 P+ R}  \ \ where \ \ \beta^2= \frac{1-\alpha}{\alpha}
\end{equation}
\noindent In the \textbf{F measure} equation (\ref{eq:fscore}), the $\alpha$ parameter tells us how much we should emphasize precision at the expense of recall: we can achieve \textit{symmetric} weights only by setting $\alpha= \frac{1}{2}$ (that implies $\beta=1$) and we refer to this peculiar F-measure as \textbf{balanced F measure} or \textbf{F-1 score} (see eq. \ref{eq:f1}).
\begin{equation}\label{eq:f1}
	F_{\beta = 1} = \frac{2PR}{P + R}
\end{equation}

\subsubsection{Choosing the optimum k}
The best way to select the optimum $k$ in our classifier is to do find its value empirically, by experimenting how it reacts with different values for $k$ in terms of our chosen evaluation metric. However, we need also to iteratively change our test and training set via \textit{cross-validation} to avoid a choice of $k$ that may be influenced by overfitting.\\
We could also make a guess for $k$ value or we could make use of any domain knowledge (knowing our data distribution or each feature importance) that we may have.\\
Still, there are some common heuristics based on common sense:
\begin{enumerate}
	\item $k$ should be greater than the number of the classes we have: if not, there would not be the possibility to represent all the classes.
	\item use an odd $k$ when we have an even number of classes such that we do not have to randomly break ties
	\item Choose a low value for $k$ in order to avoid noise or when we have an imbalanced dataset: otherwise we would always prioritize the most common class.
\end{enumerate}
Choosing a good value for $k$ can boost the performance of our classifier by operating on its algorithm but sometimes better results are achieved just increasing the size of our training set or to prune it cleverly by removing noisy training examples, meaning those instances whose $k$ nearest neighbours all belongs to another class.
Quoting Pedro Domingos in \cite{Domingos}:
\begin{framed}
\begin{quote}
	\centering
	\textquotedblleft As a rule of thumb, a dumb algorithm with lots and lots of data \\beats a clever one with modest amounts of it.\textquotedblright
\end{quote}
\end{framed}
\noindent The funniest thing is that usually in order to have bigger training sets, actual people must label the instances but in the first place we dumped them for machines to make things faster. An example of this paradox is the \emph{Amazon Mechanical Turk}\footnote{\url{https://www.mturk.com/mturk/welcome}} (which is nicely advertised as \textit{Artificial} Artificial Intelligence) where you can get paid for classification tasks or for helping with speech recognition by dubbing or writing transcripts of audio and video contents.
\subsection{Bias and Variance}
We want that the learners we devise will develop pattern recognition skills as good as (or better than) those that we are carrying in our brains: we want faster and more accurate forecasts based on the data we have.\\
What can go wrong when data are studied \emph{objectively} by computers?\\
To answer that we should remember some wild (and wrong) theories conjectured by scientists or philosophers throughout history: they had the same data than their contemporaries and sometimes they were erudite men but the way in which they interpreted that data was influenced erroneously by their prejudices.\\
Nowadays, in many languages \emph{prejudice} bears a negative connotation but it is the term that better represent the hidden influences that we have to fight while forming notions about our data. One of the father of the scientific method, Francis Bacon, called these prejudices \textit{idola}: two types of these \textit{idola} were \textit{idola specus} that come from the subjectivity of our minds and from our cultural background and \textit{idola theatri} that depends on the authority of past philosophical and scientific systems of thought that we do not want to challenge.\\
The problem with these \textit{idola} or prejudices is that they hinder the possibility of finding the real latent pattern and to bottle it up in a widely-applicable generalized formula.\\
%Both \textit{idola} may hinder the reach of a generalized formula $f(x)$ with respect to our given data because the former will lead us to pursue our goal starting from the point of view of our previous studies while the latter prevent us from experimenting something new as we simply stick to the popular theories of the moment.\\
Automated learners are imperfect as we humble human beings and they bump into other two types of prejudices: bias and variance.\\
\textbf{Bias} means that we keep learning the same wrong thing from a data while \textbf{variance} refers to when we concentrate on irrelevant aspects (e.g. noisy features) that make us loose the focus on the real signal/pattern hidden inside that data.

\section{Distributed Learning}\label{DistribLearning}
%\epigraph{\textit{Two heads are better than one}}%
%{Proverb}
%\epigraph{\textit{Ser humano consiste en la vocaci\'{o}n de compartir lo que ya sabemos entre todos, ense\~{n}ando a los reci\'{e}n llegados al grupo cuanto deben conocer para hacerse socialmente v\'{a}lidos}}%
%{Fernando Savater  - \emph{El valor de educar}}\footnote{To be human means to be inclined to share our knowledge with everyone, teaching to those who have only recently become a part of our group what they must know in order to be useful to the society}
%\subsection{Definition and Issues}
A commonplace problem is to effectively and accurately learn from data scattered around independent locations: these entities could belong to the same organization but they may be geographically distant or they could be part of different enterprises that may want to collaborate by sharing a small part of their knowledge. \\ 
To devise a learner that has to overcome these problem is troublesome as we may have restraints on the exchange of data between the interested parties because of privacy concerns (if the stored data contains personal data) or of communications boundaries (limited bandwidth could make the transferring of sizeable datasets infeasible).\\
Assuming that the distributed datasets are \textbf{homogeneous}\footnote{Meaning that we store the same features for every instance. In case of heterogeneous sources we would need to look into an unambiguous ontology to describe the instance such that it would be possible to extract only the desired common features.} we may discern two kinds of fragmentation according to how instances are split (fig. \ref{fig:data_fragm}): data is \textbf{vertically distributed} if we separate the dataset by choosing a range of columns (fig. \ref{fig:data_fragm}(a))\footnote{We end up with subtuples of the whole tuple} otherwise it is defined as \textbf{horizontally distributed} (fig. \ref{fig:data_fragm}(b)) if we pick just a subset of rows where tuples are stored in their entirety.
\begin{figure}[h!]
	\centering
	\subfloat[Vertically distributed]{{\includegraphics[width=0.415\textwidth]{images/vertFrag}}}%
	\qquad
	\subfloat[Horizontally distributed]{{\includegraphics[width=0.45\textwidth]{images/horizFrag}}}%
	\caption{Data fragmentation cases}%
	\label{fig:data_fragm}%
\end{figure}
\\The most straightforward answer to the \textbf{distributed learning} problem is to create local learners, make them evaluate the query and combining the results usually by means of a majority vote: although this approach may sound too elementary, it carries out its task as well as other more complex learners especially when skewed data is involved \cite{EffectivenessDistLearn}.\\
This type of approaches is known as \textbf{ensemble methods} \cite{SurveyDML}: we either combine the different predictions or we aggregate the learners/classifiers themselves.\\
To converge into a single classification label from multiple local computations, we have chosen the first strategy.\\
We must give to our learner $L$ (be it distributed or not) enough information to guide $L$ towards the formulation of the correct hypothesis $h$: in \cite{PHD_Caragea} this amount of data is defined as \textit{\textbf{sufficient statistics for learning}} $s_L(D)$\footnote{D stands for our dataset} thus it follows that $L$ will output $h$ when $s_L(D)$ is given as input.\\
In a \textsf{KNN} classifier, the $k$-nearest neighbours or even just the list of their related $k$ $<distance,\ label>$ tuples (represented in our code with the case class \textsf{DistanceWithLabel}, see \S \ref{knn_spark_intro}) are by all means \textit{\textbf{sufficient statistics}} because the attribution of a class label by means of a majority vote depends on them, not on the whole dataset ($D$).\\
By exchanging only the $<distance,\ label>$ tuples, we decrease the communication costs and we guarantee \textit{privacy} with regard to the instances belonging to the datasets.\\
We aim for algorithms providing \textbf{\textit{exact}} distributed learning, meaning that we want the decentralized computation to output the same result for our query of its centralized counterpart, but in order to do that we should define a rule to follow when breaking ties instead of the usual random choice \cite{PHD_Caragea}.\\ 
Even if we implement this last restriction, our distributed learner model will be exact if and only if all the nodes that store the partitioned data will help us with their intermediate outcomes.\\
It was proved (Theorem 3.47 in \cite{PHD_Caragea}) that since these partial results are the \textit{local} nearest neighbours, by performing their union we will be able to find the \textit{global} nearest neighbours that we would get with a centralized batch processing.\\
In order to test our \textsf{CollaborativeCluster}, we have simulated a horizontal distribution of our training sets with different degrees of \textit{data skewness} and our results will be presented in Chapter \ref{conclusion_chapter}.\\