\begin{savequote}[100mm]
	\textquotedblleft Innumerable confusions and a profound feeling of despair invariably emerge in periods of great technological and cultural transitions.\\
	 Our Age of Anxiety is, in great part, the result of trying to do\\ today's job with yesterday's tools and yesterday's concepts.\textquotedblright
	\qauthor{\scriptsize Marshall McLuhan, \emph{The Medium is the Message: An Inventory of Effects}}
\end{savequote}

\chapter[Apache Flink and Spark code]{A brief look into Apache Spark and Flink code}\label{CH_BDCODE}
\section{No MapReduce before-hand knowledge is required}\label{NoKnowlReq}
In order to get started with \emph{Apache Spark} and \emph{Flink}, you do not need to be familiar with the map-reduce paradigm because both frameworks shifted from a customizable sequence of two-phases\footnote{Eventually three, if you add a combine step between the map and reduce phase} jobs into a cascade of freely outlined jobs that may have iterations in them (e.g. a \textit{k-means} clustering).\\
What is helpful to know is the logic that Yarn and other resource-management tools use to determine how tasks and data get distributed into the cluster, whether it is physical or logical (see \S \ref{Yarn}).\\
The actual \textsf{map} and \textsf{reduce} calls in Spark and Flink are closer to their Scala counterparts than the traditional Hadoop ones: for example \textsf{reduce} comes after a shifting phase as in Hadoop but is compelled to give as output the same data type of the value inside the \textit{key-value} pairs that compose a Dataset.
\section{KNN with Spark}\label{knn_spark_intro}
Spark enters the data read from input into collections of elements called \textsf{Resilient Distributed Datasets} (\textsf{RDD}s) that are split such that each node of the cluster contains a partition of the whole data.\\
If an operation causes the creation of another \textsf{RDD}s, the operation will be defined \textit{transformation} otherwise if its outcome is the computation of a result related to the data contained in a \textsf{RDD}, we refer to it as an \textit{action} (e.g. \textsf{saveAsTextFile}, \textsf{first}, \textsf{count}, \textsf{take}).
Only actions trigger the execution of the Spark code while transformations will be evaluated in a lazy fashion: meaning only when we invoke an action that depends on them.\\
\emph{Apache Spark} has an extensive Machine Learning library called \textsf{MLlib} and in order to handle instances it uses a \textsf{LabeledPoint} type that contains a double to store the class' label and a Vector of Doubles (programmers can choose between a dense, \textsf{DenseVector}, and a sparse, \textsf{SparseVector}, representation).\\
To compute Gower distance, we need the value range for all the features and this could be easily achieved through a library of statistical tools (\textsf{MultivariateStatisticalSummary}) that implements a fast way to extract meaningful information such as the \textit{minimum} and \textit{maximum} values for each column/feature about the whole dataset by calling \textsf{Statistics.colStats(}\textit{datasetName}\textsf{)}.\\
We will create a \textsf{case class} (\textsf{DistanceWithLabel} in the code) to store tuples of type $<distance,\ label>$ over which the ordering it is defined with respect only to the distance item.
\subsection{Thyroid: Simplest possible KNN}\label{spark_thy}
Since Thyroid dataset is quite small in terms of memory requirements we can simply perform a map operation producing tuples of class \textsf{DistanceWithLabel}, order all them by the distance field and take only the \textit{closest}\footnote{Smallest values if the metric is a distance, highest values if is a similarity} $k$ instances.\\
We produce as output only pairs of \textsf{DistanceWithLabel} items in a CSV\footnote{The format of the output file is following the comma separated value template but its actual name will be \emph{part\_00000} (parallelism is set to one, so we expect just one writer and one output file) because it will be written as an Hadoop output file} file: the latest step, the assignment of a label, is performed later when all the remote workers have carried out their tasks and it is explained in \S\ref{collecting_results_scala}.
\subsection{SUSY: need for enhanced KNN}\label{spark_susy}
What happens when we move from thousands of instances to hundreds of thousands or millions? Do we really need to sort all the whole dataset of \textsf{DistanceWithLabel} tuples given its size? Priority queues come in handy: we ask to compute the distance of all the items in the test set but we store just the closest $k$ using a dynamic priority queue. We will have as many priority queues as many partitions have been instantiated so those intermediate results need to be merged.\\
We use a \textsf{mapPartition} that applies the distance function to all the elements in the partitions and allows the cardinality of the resulting RDD to be different from the source RDD (our training set).\\
It was necessary a \textsf{mapPartition} operation because the size of our outcome (the priority queues) is smaller than the input while a \textsf{map} essentially converts \textit{each} element of the source RDD into \textit{a single} element of the resulting RDD.
%because each partition's priority queue is our outcome and their size is way smaller than our RDD with training instances 
\section[Accuracy over whole test set]{Calculating accuracy over the whole test set}\label{accuracy_whole}
In order to rate the accuracy of our classifier we need to evaluate it over all the instances in the test set: the first though would be to simply ask to perform a map over the \textsf{RDD} containing the test examples and inside the map function ask to calculate the distance with respect to all the instances in the training set \textsf{RDD} resulting in a sort of \textit{nested} map (essentially, we put an inner map function inside an outer map).\\
Unfortunately, \textsf{RDD}s are logical and Spark decides the execution order of the operations by checking their \textit{dependency graph} and inferring the best schedule for resources management and a \textit{map} function inside a \textit{map}
%creates a sort of logical loop in the second phase thus 
results in an exception thrown at runtime that states:
\begin{quotation}
	\small
	\textquotedblleft\noindent\textsf{\textit{RDD transformations and actions can only be invoked by the driver, not inside of other transformations; for example, rdd1.map($x => $rdd2.values.count() $*$ x) is invalid because the values transformation and count action cannot be performed inside of the rdd1.map transformation.}}\textquotedblright
\end{quotation}
A solution, as pointed up by Parsian in Chapter 13 of \cite{Parsian}, is to perform a Cartesian Product between the training set and the test set. At first it seems plain overkill, but you need to think that Spark \textquotedblleft\textbf{auto\textit{magically}}\textquotedblright\ optimizes the steps to be performed and that the transformations are \textit{logical} operations so how a particular operation is carried out can greatly differ from what we expect.\\
What is an unnecessary waste is the call \textsf{groupByKey()} to group together all the neighbours' distances related to the identifier of an item: with \textsf{groupByKey()} all the key-value pairs are shuffled around and this means that data is being transferred over the network.\\
Let us do a back-of-the-envelope calculation with respect to the SUSY dataset: the cardinality of the Cartesian product $C$ between the 1000 (1K) test examples and the 4500000 (4.5M) training set instances is in the order of billions (4.5G) objects and even if what we would move around the network are just the tuples of type \textsf{$<$ItemWithLabel, DistanceWithLabel$>$}\footnote{Note: the tuples equivalent to the case classes would be of type \textsf{$<<$\textit{Index}:Long, \textit{Label}:Double$>$, $<$\textit{Distance}:Double, \textit{Label}:Double$>>$} }achieved after a \textsf{map} over $C$'s items, this means that the input of the shuffling phase is roughly of 32 Bytes\footnote{In Scala and Java both Long and Double values are accommodated in 64-bit thus each tuple weights \textit{at least} 8 Bytes *4 = 32 Bytes} $\cdot$ 4.5G tuples...that settles it around 134 GB\footnote{This calculation does not take into account the redundancy copies of the blocks (usually three) that should be created inside Spark HDFS for fault tolerance purposes}.\\
Desperate times call for desperate measures: we want definitely to avoid the \textsf{shuffle} after the \textsf{map} phase that creates this monstrous dataset.\\
We will use a \textsf{combine} function that essentially resembles a \textsf{reduce} function in MapReduce: we start from the iterable collection of the Cartesian product tuples and we apply a custom function such that for each key the outcome is another iterable collection of different type (\textsf{DistanceWithLabel}).\\
In order to operate a combine we need \textit{three} functions: one that initializes the output collection, another one that states what happens in every partition and a last one that sets the rule for collecting and combining the intermediate results.\\
Then, we select by majority voting over the $k$ nearest neighbours the class label and if it corresponds to the actual one, we increment the \textsf{accumulator} storing the correct classifications: we are compelled to use \textsf{accumulators} as we need shared registers whose values can be safely modified also in case of multiple workers executing in parallel.

\section{Collecting Results}\label{collecting_results_scala}
Inside the \textsf{CollaborativeCluster}, all the nodes that decided to help either with the SUSY or with the Thyroid dataset run programs written specifically either for \emph{Spark} (\S \ref{spark_thy} and \S \ref{spark_susy}) or for \emph{Flink} (that version will be discussed in \S \ref{knn_flink}) whose outcome is a sequence of objects of class \textsf{DistanceWithLabel} (meaning a \textsf{$<$distance: Double, label:Double$>$} tuple) in order to not give away anything about the instances concerning their training set.\\
Our \textsf{MajordomoActor} (\S \ref{majordomoactor}) then will gather all these partial results and run a small Scala program executing sequentially these actions:
\begin{enumerate}
	\item it will read all the files and sort the instances according to the metric either in \textit{ascending} (if we deal with \textit{distances}) or \textit{descending} (if we deal with percentages of \textit{similarity}) order;
	\item it will select among the global results the $k$ nearest neighbours;
	\item it will perform a majority voting to decide the class label.
\end{enumerate}
Since we want to implement an \textbf{exact} distributed learning algorithm (see \S \ref{DistribLearning}) and for breaking ties is not allowed a random choice, we will take from the \textit{ex aequo} contenders only the class with the smallest label, see code in Listing \ref{CollectCode}. 
\lstset{language=scala,caption={Breaking ties during classification in the \textsf{collect} code},label=CollectCode}
\begin{lstlisting}[firstnumber=96]
	/** The variable _result_ contains the classification label achieved by 
	* gathering all the outcomes of our local KNN executions. 
	* If k=1 we need to retrieve only the label of the global nearest neighbour.
	* Otherwise we assign the label by performing a majority vote but since we 
	* want *exact* distributed learning we need to devise a way to breaking ties 
	* that remains the same in every execution (no random choices allowed).
	* Note:No statistics wrt the dataset are available (frequency of each class)
	* */
	val result: Double =
		if(k_from_args = = 1) {
		//pq is a priority queue containing the union of the local results 
		//whose ordering is set according to the 
		//used metric (Distance or Similarity)
			pq.dequeue()._2
		}
		else{
			val orderingByMajorityVote: Ordering[(Int,Double)] = 
				Ordering.by((_: (Int, Double))._1).reverse
			val kValues= for(index <- 0 until k_from_args) yield pq.dequeue()
			//Group the results by the label (1),
			//count the instances for each label (2),
			//and use this value as the new key (3)
			val groupedVotes= kValues.groupBy(_._2) //(1)
				.map(z => (z._1, z._2.size)) //(2)
				.toSeq.map(x => x.swap) //(3)
			//Sort by number of instances per class
			val sortedVotes= groupedVotes.sorted(orderingByMajorityVote)
			//Check the maximum amount of votes received...
			val best_res= sortedVotes.max._1
			//...and see if there are ties to break
			val exAequo= sortedVotes.filter(_._1 == best_res)
			//Select among the contenders the class with the smallest label
			exAequo.sortBy(_._2).take(1).head._2
		}
\end{lstlisting}

\section{KNN with Flink}\label{knn_flink}
Once you get accustomed to \emph{Apache Spark} API, it should not take too much effort to write code for \emph{Apache Flink}.\\
For example, the execution context is called \textsf{env} instead of \textsf{sc} thus it would seem enough just to find out how the Spark methods counterparts  are called in this framework: nevertheless, \emph{Apache Flink} is a hefty rival whose memory management and serialization is outstanding and it even wins the performance comparison with a mature and successful project like \emph{Spark}.\\
\emph{Apache Flink} offers also a handy parser called \textsf{ParameterTool} that can extract the configuration details either from command line or from files following the \textsf{java.util.Properties} standard. Flink's Machine Learning library provides an equivalent of Spark's \textsf{Labeled\-Point} under the name of \textsf{LabeledVector} whose implementation is deeply intertwined with \emph{Breeze}\footnote{Breeze is a library for Scala designed to help with  machine learning and numerical processing tasks (its repository on Github is \url{https://github.com/scalanlp/breeze})} data structures.\\
Unfortunately we had to devise ourselves the way to extract the range for the features and since calculating it for each query (even when no change to the training set has been made) represented a clearly avoidable overhead we added the possibility of reading it from a previously stored computation.\\
It was straightforward to adapt the rest of the code to run on \emph{Apache Flink} and to follow at the same time the approach with priority queues used in \S  \ref{spark_susy}.\\
For further details on the performance of these \textsf{KNN} jobs in terms of execution time, please refer to Chapter \ref{conclusion_chapter}.

 